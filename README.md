# hero-app

Hero é uma aplicação de gestão de hábitos, onde o usuário pode cadastrar novos hábitos a serem cumpridos periodicamente, participar de grupos e fazer atividades coletivas.

## Uso

Para começar a usar a aplicação localmente, faça um clone e rode os seguintes comandos:  
`yarn` Para atualizar as libs  
`yarn start` Para começar a servir o projeto localmente  

Clique [**AQUI**](https://heroapp-delta.vercel.app/) para acessar o app no vercel.
