import api from "../../services/api";
import { FormControl, FormLabel, Stack, useToast, Box } from "@chakra-ui/react";
import { CheckCircleIcon, WarningIcon } from "@chakra-ui/icons";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import { Error, Form, StyledInput, StyledButton } from "./styles";
import { addActivitySchema } from "../../schemas/schemas";
import { useUser } from "../../providers/user";
import { useUserGroups } from '../../providers/groups'

const AddActivity = ({group, setGroup }) => {
	
	const toast = useToast();
	const {
		handleSubmit,
		register,
		formState: { errors: formErrors },
	} = useForm({ resolver: yupResolver(addActivitySchema) });

	const { user: { token } } = useUser();

	const { userGroupsList, setUserGroupsList } = useUserGroups();

	const AddActivity = data => {

		data.realization_time = new Date(data.realization_time).toISOString()
		data.group = group.id
		api
			.post("/activities/", data, {
				headers: {
					Authorization: `Bearer ${token}`,
				},
			})
			.then( response => {
				const data = response.data;
				setGroup({...group, activities: [...group.activities, data ] });
				const newGpList = userGroupsList.map( gp => {
					if(gp.id === data.group){
						return {
							...gp,
							activities: [...gp.activities, data]
						}
					}else{
						return gp
					}
				})
				setUserGroupsList(newGpList);
				
				toast({
					position: "top-right",
					duration: 1500,
					render: () => (
						<Box
							color="white.500"
							p={4}
							display="flex"
							alignItems="center"
							bg="green.300"
						>
							<CheckCircleIcon mr={2.5} />
							Atividade criada com sucesso
						</Box>
					),
				});
			})
			.catch((e) =>
				toast({
					position: "top-right",
					duration: 1500,
					render: () => (
						<Box
							color="white.500"
							p={4}
							display="flex"
							alignItems="center"
							bg="red.300"
						>
							<WarningIcon mr={2.5} />
							Não foi possível criar Meta
						</Box>
					),
				}),
			);
	};

	return (
		<Form onSubmit={handleSubmit(AddActivity)}>
			<Stack
				margin="auto"
				color="white.500"
				maxWidth="400"
				width="85%"
				minWidth="250"
				spacing={3}
				padding="1rem 0"
				boxSizing="content-box"
			>
				<FormControl>
					<FormLabel fontWeight="600" fontSize="sm" htmlFor="title">
						Atividade
					</FormLabel>
					<StyledInput
						color="white.500"
						type="text"
						id="title"
						colorScheme="green"
						{...register("title")}
						placeholder="sua atividade que deseja realizar ..."
						borderColor={formErrors.title ? "red.300" : "white.500"}
					/>
					<Error className={formErrors.title ? "show" : ""}>
						{formErrors.title ? formErrors.title.message : "..."}
					</Error>
				</FormControl>

				<FormControl>
					<FormLabel fontWeight="600" fontSize="sm" htmlFor="difficulty">
						Tempo para a Atividade
					</FormLabel>
					<StyledInput
						color="white.500"
						type="date"
						id="title"
						colorScheme="green"
						{...register("realization_time")}
						borderColor={formErrors.realization_time ? "red.300" : "white.500"}
					/>
					<Error className={formErrors.realization_time ? "show" : ""}>
						{formErrors.realization_time ? formErrors.realization_time.message : "..."}
					</Error>
				</FormControl>

				<FormControl display="flex" flexDirection="column" alignItems="center">
					<StyledButton
						type="submit"
						variant="outline"
						color="white.500"
						background="transparent"
					>
						ADD
					</StyledButton>
				</FormControl>
			</Stack>
		</Form>
	);
};

export default AddActivity;
