import api from "../../services/api";
import {
  FormControl,
  FormLabel,
  Stack,
  Select,
  useToast,
  Box,
} from "@chakra-ui/react";
import { CheckCircleIcon, WarningIcon } from "@chakra-ui/icons";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import { Error, Form, StyledInput, Option, StyledButton } from "./styles";
import { addGoalSchema } from "../../schemas/schemas";
import { useUser } from "../../providers/user";
import { difficultyLevels } from "../../utils";
import { useUserGroups } from "../../providers/groups";

const AddGoal = ({ group, setGroup }) => {
  const {
    HERO_VERY_EASY,
    HERO_EASY,
    HERO_INTERMEDIARY,
    HERO_HARD,
    HERO_VERY_HARD,
  } = difficultyLevels;

  const toast = useToast();
  const {
    handleSubmit,
    register,
    formState: { errors: formErrors },
  } = useForm({ resolver: yupResolver(addGoalSchema) });

  const {
    user: { token },
  } = useUser();
  const { userGroupsList, setUserGroupsList } = useUserGroups();

  const AddGoal = (data) => {
    data.how_much_achieved = 0;
    data.group = group.id;

    api
      .post("/goals/", data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        const data = response.data;
        setGroup({ ...group, goals: [...group.goals, data] });
        const newGpList = userGroupsList.map((gp) => {
          if (gp.id === data.group) {
            return {
              ...gp,
              goals: [...gp.goals, data],
            };
          } else {
            return gp;
          }
        });
        setUserGroupsList(newGpList);

        toast({
          position: "top-right",
          duration: 1500,
          render: () => (
            <Box
              color="white.500"
              p={4}
              display="flex"
              alignItems="center"
              bg="green.300"
            >
              <CheckCircleIcon mr={2.5} />
              Meta criado com sucesso
            </Box>
          ),
        });
      })
      .catch((error) => {
        toast({
          position: "top-right",
          duration: 1500,
          render: () => (
            <Box
              color="white.500"
              p={4}
              display="flex"
              alignItems="center"
              bg="red.300"
            >
              <WarningIcon mr={2.5} />
              Não foi possível criar Meta
            </Box>
          ),
        });
      });
  };

  return (
    <Form onSubmit={handleSubmit(AddGoal)}>
      <Stack
        margin="auto"
        color="white.500"
        maxWidth="400"
        width="85%"
        minWidth="250"
        spacing={3}
        padding="1rem 0"
        boxSizing="content-box"
      >
        <FormControl>
          <FormLabel fontWeight="600" fontSize="sm" htmlFor="title">
            Meta
          </FormLabel>
          <StyledInput
            color="white.500"
            type="text"
            id="title"
            colorScheme="green"
            {...register("title")}
            placeholder="sua meta que deseja atingir ..."
            borderColor={formErrors.title ? "red.300" : "white.500"}
          />
          <Error className={formErrors.title ? "show" : ""}>
            {formErrors.title ? formErrors.title.message : "..."}
          </Error>
        </FormControl>

        <FormControl>
          <FormLabel fontWeight="600" fontSize="sm" htmlFor="difficulty">
            Nível de dificuldade
          </FormLabel>
          <Select
            color="white.500"
            type="text"
            id="difficulty"
            colorScheme="green"
            {...register("difficulty")}
            borderColor={formErrors.difficulty ? "red.500" : "white.500"}
          >
            <Option defaultValue value={HERO_VERY_EASY}>
              Muito fácil
            </Option>
            <Option value={HERO_EASY}> Fácil </Option>
            <Option value={HERO_INTERMEDIARY}> Intermediário </Option>
            <Option value={HERO_HARD}> Difícil </Option>
            <Option value={HERO_VERY_HARD}> Muito Difícil </Option>
          </Select>
          <Error className={formErrors.difficulty ? "show" : ""}>
            {formErrors.difficulty ? formErrors.difficulty.message : "..."}
          </Error>
        </FormControl>

        <FormControl display="flex" flexDirection="column" alignItems="center">
          <StyledButton
            type="submit"
            variant="outline"
            color="white.500"
            background="transparent"
          >
            ADD
          </StyledButton>
        </FormControl>
      </Stack>
    </Form>
  );
};

export default AddGoal;
