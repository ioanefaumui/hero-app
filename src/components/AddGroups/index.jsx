import api from "../../services/api";
import {
  FormControl,
  FormLabel,
  Stack,
  Select,
  useToast,
  Box,
} from "@chakra-ui/react";
import { CheckCircleIcon, WarningIcon } from "@chakra-ui/icons";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import { Error, Form, StyledInput, Option, StyledButton } from "./styles";
import { addGroupsSchema } from "../../schemas/schemas";
import { useUser } from "../../providers/user";
import { groupsCategories } from "../../utils";

const AddGroups = ({ loadGroups }) => {
  const {
    HERO_EDUCATION,
    HERO_EXERCISES,
    HERO_HEALTH,
    HERO_KITCHEN,
    HERO_LEISURE,
    HERO_MUSIC,
    HERO_OTHER,
    HERO_READING,
    HERO_SPORT,
    HERO_TRIP,
  } = groupsCategories;

  const toast = useToast();
  const {
    handleSubmit,
    register,
    formState: { errors: formErrors },
    reset,
  } = useForm({ resolver: yupResolver(addGroupsSchema) });

  const {
    user: { token },
  } = useUser();

  const AddGroup = (data) => {
    api
      .post("/groups/", data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(() => {
        loadGroups();
        reset();
        toast({
          position: "top-right",
          duration: 1500,
          render: () => (
            <Box
              color="white.500"
              p={4}
              display="flex"
              alignItems="center"
              bg="green.300"
            >
              <CheckCircleIcon mr={2.5} />
              Grupo criado com sucesso.
            </Box>
          ),
        });
      })
      .catch(() =>
        toast({
          position: "top-right",
          duration: 1500,
          render: () => (
            <Box
              color="white.500"
              p={4}
              display="flex"
              alignItems="center"
              bg="red.300"
            >
              <WarningIcon mr={2.5} />
              Não foi possível criar o grupo.
            </Box>
          ),
        })
      );
  };

  return (
    <Form onSubmit={handleSubmit(AddGroup)}>
      <Stack
        margin="auto"
        color="white.500"
        maxWidth="400"
        width="85%"
        minWidth="250"
        spacing={3}
        padding="1rem 0"
        boxSizing="content-box"
      >
        <FormControl>
          <FormLabel fontWeight="600" fontSize="sm" htmlFor="title">
            Nome do grupo
          </FormLabel>
          <StyledInput
            color="white.500"
            type="text"
            id="name"
            colorScheme="green"
            {...register("name")}
            placeholder="Ex: Grupo de Leitura, Grupo de Corrida ..."
            borderColor={formErrors.name ? "red.300" : "white.500"}
          />
          <Error className={formErrors.name ? "show" : ""}>
            {formErrors.title ? formErrors.name.message : "..."}
          </Error>
        </FormControl>

        <FormControl>
          <FormLabel fontWeight="600" fontSize="sm" htmlFor="title">
            Descrição do grupo
          </FormLabel>
          <StyledInput
            color="white.500"
            type="text"
            id="description"
            colorScheme="green"
            {...register("description")}
            placeholder="Ex: Realizar corridas de curta e de longa distância ..."
            borderColor={formErrors.description ? "red.300" : "white.500"}
          />
          <Error className={formErrors.description ? "show" : ""}>
            {formErrors.title ? formErrors.description.message : "..."}
          </Error>
        </FormControl>

        <FormControl>
          <FormLabel fontWeight="600" fontSize="sm" htmlFor="category">
            Categoria
          </FormLabel>
          <Select
            color="white.500"
            type="text"
            id="category"
            colorScheme="green"
            {...register("category")}
            borderColor={formErrors.category ? "red.500" : "white.500"}
          >
            <Option defaultValue value={HERO_EDUCATION}>
              {" "}
              Educação{" "}
            </Option>
            <Option value={HERO_READING}> Leitura </Option>
            <Option value={HERO_EXERCISES}> Exercícios </Option>
            <Option value={HERO_SPORT}> Esporte </Option>
            <Option value={HERO_LEISURE}> Lazer </Option>
            <Option value={HERO_HEALTH}> Saúde </Option>
            <Option value={HERO_MUSIC}> Música </Option>
            <Option value={HERO_KITCHEN}> Cozinha </Option>
            <Option value={HERO_TRIP}> Viagem </Option>
            <Option value={HERO_OTHER}> Outra </Option>
          </Select>
          <Error className={formErrors.category ? "show" : ""}>
            {formErrors.category ? formErrors.category.message : "..."}
          </Error>
        </FormControl>

        <FormControl display="flex" flexDirection="column" alignItems="center">
          <StyledButton
            type="submit"
            variant="outline"
            color="white.500"
            background="transparent"
          >
            ADD
          </StyledButton>
        </FormControl>
      </Stack>
    </Form>
  );
};

export default AddGroups;
