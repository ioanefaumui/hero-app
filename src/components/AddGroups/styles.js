import styled from "styled-components";
import {
  Input,
  Button
} from "@chakra-ui/react";

export const Error = styled.span`
  font-size: .8rem;
  color: #ff4540;
  opacity: 0;
  transition: .5s;

  &.show{
    opacity: 1;
    transition: .5s;
  }
`;

export const Form = styled.form`
  width: 100%;
  height: 400px;
  display: flex;
  align-items: center;
  justify-content: center;
  background: #319795;
`;

export const StyledInput = styled(Input)`
  &::placeholder{
    color: #e5e5e5;
  }
`;

export const Option = styled.option`
  color: #000;
  /* background: #148f8c !important; */
  background: #fff !important;
  font-weight: 600;
`;

export const StyledButton = styled(Button)`
  &:hover{
    color: #319795;
  }
`;