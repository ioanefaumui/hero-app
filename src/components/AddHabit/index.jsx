import api from "../../services/api";
import {
  FormControl,
  FormLabel,
  Stack,
  Select,
  useToast,
  Box,
} from "@chakra-ui/react";
import { CheckCircleIcon, WarningIcon } from "@chakra-ui/icons";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import { Error, Form, StyledInput, Option, StyledButton } from "./styles";
import { addHabitSchema } from "../../schemas/schemas";
import { useUser } from "../../providers/user";
import {
  groupsCategories,
  difficultyLevels,
  TAG_USER_HABITS_LIST,
  toastConfig,
} from "../../utils";

const AddHabit = ({ setIsToRender, userHabitsList, setUserHabitsList }) => {
  const {
    HERO_VERY_EASY,
    HERO_EASY,
    HERO_INTERMEDIARY,
    HERO_HARD,
    HERO_VERY_HARD,
  } = difficultyLevels;

  const {
    HERO_EDUCATION,
    HERO_EXERCISES,
    HERO_HEALTH,
    HERO_KITCHEN,
    HERO_LEISURE,
    HERO_MUSIC,
    HERO_OTHER,
    HERO_READING,
    HERO_SPORT,
    HERO_TRIP,
  } = groupsCategories;

  const toast = useToast();
  const {
    handleSubmit,
    register,
    formState: { errors: formErrors },
    reset,
  } = useForm({ resolver: yupResolver(addHabitSchema) });

  const {
    user: { token, id },
  } = useUser();

  const AddHabit = (data) => {
    const {
      DURATION,
      POSITION,
      COLOR,
      PADDING,
      DISPLAY,
      ALIGN_ITEMS,
      MARGIN_RIGHT,
      GREEN,
      RED,
    } = toastConfig;

    data.achieved = false;
    data.how_much_achieved = 0;
    data.user = id;

    api
      .post("/habits/", data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        const { data } = response;

        setUserHabitsList([...userHabitsList, data]);

        localStorage.setItem(
          TAG_USER_HABITS_LIST,
          JSON.stringify(userHabitsList)
        );

        reset();

        setIsToRender(true);
        toast({
          position: POSITION,
          duration: DURATION,
          render: () => (
            <Box
              color={COLOR}
              p={PADDING}
              display={DISPLAY}
              alignItems={ALIGN_ITEMS}
              bg={GREEN}
            >
              <CheckCircleIcon mr={MARGIN_RIGHT} />
              Hábito criado com sucesso.
            </Box>
          ),
        });
      })
      .catch(() =>
        toast({
          position: POSITION,
          duration: DURATION,
          render: () => (
            <Box
              color={COLOR}
              p={PADDING}
              display={DISPLAY}
              alignItems={ALIGN_ITEMS}
              bg={RED}
            >
              <WarningIcon mr={MARGIN_RIGHT} />
              Não foi possível criar hábito.
            </Box>
          ),
        })
      );
  };

  return (
    <Form onSubmit={handleSubmit(AddHabit)}>
      <Stack
        margin="auto"
        color="white.500"
        maxWidth="400"
        width="85%"
        minWidth="250"
        spacing={3}
        padding="1rem 0"
        boxSizing="content-box"
      >
        <FormControl>
          <FormLabel fontWeight="600" fontSize="sm" htmlFor="title">
            Nome do hábito
          </FormLabel>
          <StyledInput
            color="white.500"
            type="text"
            id="title"
            colorScheme="green"
            {...register("title")}
            placeholder="Ex: correr, malhar, ler ..."
            borderColor={formErrors.title ? "red.300" : "white.500"}
          />
          <Error className={formErrors.title ? "show" : ""}>
            {formErrors.title ? formErrors.title.message : "..."}
          </Error>
        </FormControl>

        <FormControl>
          <FormLabel fontWeight="600" fontSize="sm" htmlFor="category">
            Categoria
          </FormLabel>
          <Select
            color="white.500"
            type="text"
            id="category"
            colorScheme="green"
            {...register("category")}
            borderColor={formErrors.category ? "red.500" : "white.500"}
          >
            <Option defaultValue value={HERO_EDUCATION}>
              {" "}
              Educação{" "}
            </Option>
            <Option value={HERO_READING}> Leitura </Option>
            <Option value={HERO_EXERCISES}> Exercícios </Option>
            <Option value={HERO_SPORT}> Esporte </Option>
            <Option value={HERO_LEISURE}> Lazer </Option>
            <Option value={HERO_HEALTH}> Saúde </Option>
            <Option value={HERO_MUSIC}> Música </Option>
            <Option value={HERO_KITCHEN}> Cozinha </Option>
            <Option value={HERO_TRIP}> Viajem </Option>
            <Option value={HERO_OTHER}> Outra </Option>
          </Select>
          <Error className={formErrors.category ? "show" : ""}>
            {formErrors.category ? formErrors.category.message : "..."}
          </Error>
        </FormControl>

        <FormControl>
          <FormLabel fontWeight="600" fontSize="sm" htmlFor="difficulty">
            Nível de dificuldade
          </FormLabel>
          <Select
            color="white.500"
            type="text"
            id="difficulty"
            colorScheme="green"
            {...register("difficulty")}
            borderColor={formErrors.difficulty ? "red.500" : "white.500"}
          >
            <Option defaultValue value={HERO_VERY_EASY}>
              Muito fácil
            </Option>
            <Option value={HERO_EASY}> Fácil </Option>
            <Option value={HERO_INTERMEDIARY}> Intermediário </Option>
            <Option value={HERO_HARD}> Difícil </Option>
            <Option value={HERO_VERY_HARD}> Muito Difícil </Option>
          </Select>
          <Error className={formErrors.difficulty ? "show" : ""}>
            {formErrors.difficulty ? formErrors.difficulty.message : "..."}
          </Error>
        </FormControl>

        <FormControl>
          <FormLabel fontWeight="600" fontSize="sm" htmlFor="frequency">
            Frequência
          </FormLabel>
          <StyledInput
            color="white.500"
            type="text"
            colorScheme="green"
            id="frequency"
            {...register("frequency")}
            placeholder="Ex: Diário, 1x na Semana ..."
            borderColor={formErrors.frequency ? "red.300" : "white.500"}
          />
          <Error className={formErrors.frequency ? "show" : ""}>
            {formErrors.frequency ? formErrors.frequency.message : "..."}
          </Error>
        </FormControl>

        <FormControl display="flex" flexDirection="column" alignItems="center">
          <StyledButton
            type="submit"
            variant="outline"
            color="white.500"
            background="transparent"
          >
            ADD
          </StyledButton>
        </FormControl>
      </Stack>
    </Form>
  );
};

export default AddHabit;
