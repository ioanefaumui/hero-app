import {
	Breadcrumb,
	BreadcrumbItem,
	BreadcrumbLink,
	Text,
} from "@chakra-ui/react";
import { ChevronRightIcon } from "@chakra-ui/icons";
import { Link } from "react-router-dom";

/**
 * @param{Object}{name, path}
 */
const BreadComponent = ({ menuOptions, ...rest }) => {
	return (
		<Breadcrumb separator={<ChevronRightIcon />} mb="1rem" {...rest}>
			{menuOptions.map((item, index) => {
				const isLastIndex = index === menuOptions.length - 1;

				return (
					<BreadcrumbItem
						key={index}
						isCurrentPage={isLastIndex ? true : false}
					>
						{isLastIndex ? (
							<BreadcrumbLink as={Link} to={item.path} color="green.500">
								<Text as="h2" fontWeight="700">
									{item.name}
								</Text>
							</BreadcrumbLink>
						) : (
							<BreadcrumbLink as={Link} to={item.path}>
								{item.name}
							</BreadcrumbLink>
						)}
					</BreadcrumbItem>
				);
			})}
		</Breadcrumb>
	);
};

export default BreadComponent;
