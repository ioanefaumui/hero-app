import { IconButton } from "@chakra-ui/button";
import { Badge, Text } from "@chakra-ui/layout";
import { Box } from "@chakra-ui/layout";
import { GiCheckMark } from "react-icons/gi";
import DeleteHabitModal from "../DeleteHabitModal";
import { AchieveBar, CardList, CardSlider, StyledBox, DTCard } from "./styles";
import { editTypes, toastConfig, TAG_USER_HABITS_LIST } from "../../utils";
import api from "../../services/api";
import { useToast } from "@chakra-ui/toast";
import { CheckCircleIcon, WarningIcon } from "@chakra-ui/icons";
import { useUser } from "../../providers/user";
import { useUserGroups } from "../../providers/groups";
import { useState } from "react";
import { CircularProgress, CircularProgressLabel } from "@chakra-ui/progress";
import { Flex } from "@chakra-ui/react";
import { SCREEN_BREAKPOINT } from "../../utils";
import { useWindow } from "../../providers/window";
import { categoriesColors } from "../../utils";
import { useLocation } from "react-router-dom";
import { useEffect } from "react";

const Card = ({
  group,
  setGroup,
  type,
  item,
  setIsToRender,
  userHabitsList,
  setUserHabitsList,
  ownerList,
  setRefresh,
  className = undefined,
}) => {
  const [cardOpen, setCardOpen] = useState(false);
  const [isOwner, setIsOwner] = useState(false);
  const [categoryCollor, setCategoryCollor] = useState("");
  const location = useLocation();
  const [gpDashboard, setGpDashBoard] = useState(undefined);
  const { setUserGroupsList, userGroupsList } = useUserGroups();
  // const params = useParams();
  const {
    user: { token, id: userID },
  } = useUser();
  const toast = useToast();

  useEffect(() => {
    userGroupsList.forEach((gp) => {
      if (gp.id === item.group && type !== editTypes.TYPE_HABIT) {
        setCategoryCollor(categoriesColors[gp.category]);
        setGpDashBoard(gp);
      }
    });
    if (ownerList) {
      ownerList.forEach((group) => {
        if (item.group === group.groupId) {
          setCategoryCollor(categoriesColors[group.category]);
        }

        if (
          group.ownerId === userID &&
          item.group === group.groupId &&
          location.pathname !== "/dashboard"
        ) {
          setIsOwner(true);
        }
      });
    }
    // eslint-disable-next-line
  }, []);

  const onlyRead = () => {
    if (type !== editTypes.TYPE_HABIT) {
      if (isOwner === true) {
        return false;
      }
      return true;
    } else {
      return false;
    }
  };

  const handleCardOpen = (e) => {
    if (e.target !== undefined && typeof e.target.className === "string") {
      if (e.target.className.includes("cantChangeSlider") === false) {
        setCardOpen(!cardOpen);
      }
    }
  };

  const heroAlert = (msg, isSuccess = true) => {
    const {
      DURATION,
      POSITION,
      COLOR,
      PADDING,
      DISPLAY,
      ALIGN_ITEMS,
      MARGIN_RIGHT,
      GREEN,
      RED,
    } = toastConfig;

    if (isSuccess) {
      toast({
        position: POSITION,
        duration: DURATION,
        render: () => (
          <Box
            color={COLOR}
            p={PADDING}
            display={DISPLAY}
            alignItems={ALIGN_ITEMS}
            bg={GREEN}
          >
            <CheckCircleIcon mr={MARGIN_RIGHT} />
            {msg}
          </Box>
        ),
      });
    } else {
      toast({
        position: POSITION,
        duration: DURATION,
        render: () => (
          <Box
            color={COLOR}
            p={PADDING}
            display={DISPLAY}
            alignItems={ALIGN_ITEMS}
            bg={RED}
          >
            <WarningIcon mr={MARGIN_RIGHT} />
            {msg}
          </Box>
        ),
      });
    }
  };

  const handleEditClick = () => {
    const { TYPE_ACTIVITY, TYPE_GOAL, TYPE_HABIT } = editTypes;
    const { id, title } = item;
    let errMsg = `Erro ao tentar excluir. Por favor, `;

    switch (type) {
      case TYPE_ACTIVITY:
        api
          .patch(
            `/activities/${id}/`,
            {
              achieved: true,
            },
            {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            }
          )
          .then((response) => {
            heroAlert(`Atividade "${title}" atualizada com sucesso.`);
            const now = Date.now();
            const apiReturn = response.data.realization_time;
            const ms = new Date(apiReturn).getTime();
            if (now < ms) {
              // const diff = ms - now;
              // const days = Math.ceil(diff / (1000 * 60 * 60 * 24));
            }
          })
          .catch((error) => {
            if (error.response.status === 401) {
              errMsg += "refaça seu login.";
            } else {
              errMsg += "tente mais tarde.";
            }
            heroAlert(errMsg, false);
            console.dir(error);
          });
          setIsToRender(true);
        break;
      case TYPE_GOAL:
        api
          .patch(
            `/goals/${id}/`,
            {
              achieved: true,
            },
            {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            }
          )
          .then((response) => {
            heroAlert(`Objetivo "${title}" atualizado com sucesso.`);
            const data = response.data;
            const newGoals = group.goals.map((goal) => {
              if (goal.id === data.id) {
                return {
                  ...goal,
                  achieved: data.achieved,
                };
              }
              return goal;
            });
            setGroup({ ...group, goals: newGoals });

            const newGpList = userGroupsList.map((gp) => {
              if (gp.id === data.group) {
                return {
                  ...gp,
                  goals: newGoals,
                };
              } else {
                return gp;
              }
            });
            setUserGroupsList(newGpList);
            setIsToRender(true);
          })
          .catch((error) => {
            if (error.response.status === 401) {
              errMsg += "refaça seu login.";
            } else {
              errMsg += "tente mais tarde.";
            }
            heroAlert(errMsg, false);
            console.dir(error);
          });

        break;
      case TYPE_HABIT:
        if (!item.achieved) {
          if (item.how_much_achieved < 100) {
            item.how_much_achieved += 10;
            if (item.how_much_achieved === 100) {
              item.achieved = true;
            }
          }

          api
            .patch(
              `/habits/${id}/`,
              {
                how_much_achieved: item.how_much_achieved,
                achieved: item.achieved,
              },
              {
                headers: {
                  Authorization: `Bearer ${token}`,
                },
              }
            )
            .then(() => {
              localStorage.setItem(
                TAG_USER_HABITS_LIST,
                JSON.stringify(userHabitsList)
              );
              setIsToRender(true);
              heroAlert(`Hábito "${title}" atualizado com sucesso.`);
            })
            .catch((error) => {
              if (error.response.status === 401) {
                errMsg += "refaça seu login.";
              } else {
                errMsg += "tente mais tarde.";
              }
              heroAlert(errMsg, false);
              console.dir(error);
            });
        }
        break;
      default:
        return;
    }
  };
  const diffDate = (time) => {
    const date1 = new Date().getTime();
    const apiReturn = time;
    const ms = new Date(apiReturn).getTime();
    const diff = ms - date1;
    const days = Math.ceil(diff / (1000 * 60 * 60 * 24));
    return days;
  };

  const { pageWidth } = useWindow();
  if (pageWidth < SCREEN_BREAKPOINT) {
    return (
      <Box
        as="div"
        width="100%"
        onClick={handleCardOpen}
        className={!!className && className}
      >
        <CardList category={gpDashboard ? gpDashboard.category : item.category}>
          <CardSlider
            className="cantChangeSlider"
            opencheck={(onlyRead() === false && cardOpen).toString()}
            category={item.category}
            groupcolor={categoryCollor}
          >
            <DeleteHabitModal
              type={type}
              item={item}
              group={group}
              setGroup={setGroup}
              setRefresh={setRefresh}
              userHabitsList={userHabitsList}
              setUserHabitsList={setUserHabitsList}
              setCardOpen={setCardOpen}
            ></DeleteHabitModal>
          </CardSlider>

          <StyledBox
            onFocus={() => setCardOpen(false)}
            w="75%"
            mr="1rem"
            opencheck={(onlyRead() === false && cardOpen).toString()}
          >
            <Text as="h2" className="title" color="#414141">
              {item.title}
            </Text>

            <Badge
              category={gpDashboard ? gpDashboard.category : item.category}
              mt="8px"
            >
              {!!item.category && item.category.split("@Hero:")[1]}
              {!!gpDashboard && gpDashboard.category.split("@Hero:")[1]}
            </Badge>
            {type === editTypes["TYPE_HABIT"] && (
              <AchieveBar
                opencheck={(onlyRead() === false && cardOpen).toString()}
                value={item.how_much_achieved}
                borderRadius="0.25rem"
                mt="0.5rem"
                mb="0.5rem"
                height="0.6rem"
              />
            )}
          </StyledBox>
          {type === editTypes["TYPE_ACTIVITY"] &&
            diffDate(item.realization_time) >= 0 && (
              <Flex>
                {diffDate(item.realization_time) > 1 ? (
                  <Flex
                    w="4rem"
                    direction="column"
                    align="center"
                    justify="center"
                  >
                    <Text
                      as="p"
                      fontSize="0.5rem"
                      className="title"
                      color="#414141"
                    >
                      Faltam
                    </Text>
                    <Flex
                      bg="var(--chakra-colors-green-300)"
                      color="white.500"
                      w="1.8rem"
                      h="1.8rem"
                      fontSize="1rem"
                      fontWeight="bold"
                      align="center"
                      justify="center"
                      borderRadius="50%"
                    >
                      {diffDate(item.realization_time)}
                    </Flex>
                    <Text
                      as="p"
                      className="title"
                      fontSize="0.5rem"
                      color="#414141"
                      marginTop="0.1rem"
                    >
                      dias.
                    </Text>
                  </Flex>
                ) : (
                  <Flex
                    w="4rem"
                    direction="column"
                    align="center"
                    justify="center"
                  >
                    <Text
                      as="p"
                      fontSize="0.5rem"
                      className="title"
                      color="#414141"
                    >
                      Falta
                    </Text>
                    <Flex
                      bg="var(--chakra-colors-red-300)"
                      color="white.500"
                      w="1.8rem"
                      h="1.8rem"
                      fontSize="1rem"
                      fontWeight="bold"
                      align="center"
                      justify="center"
                      borderRadius="50%"
                    >
                      {diffDate(item.realization_time)}
                    </Flex>
                    <Text
                      as="p"
                      className="title"
                      fontSize="0.5rem"
                      color="#414141"
                      marginTop="0.1rem"
                    >
                      dia.
                    </Text>
                  </Flex>
                )}
              </Flex>
            )}

          {type === editTypes["TYPE_ACTIVITY"] &&
            diffDate(item.realization_time) < 0 && (
              <Flex
                w="4rem"
                direction="column"
                align="center"
                justify="center"
                color="red.500"
              >
                <Flex
                  bg="var(--chakra-colors-red-500)"
                  color="white.500"
                  p=".3rem"
                  mr=".3rem"
                  h="1.8rem"
                  fontSize="0.8rem"
                  fontWeight="bold"
                  align="center"
                  justify="center"
                  borderRadius="5%"
                >
                  Expirou
                </Flex>
              </Flex>
            )}
          {onlyRead() === false && (
            <Box className="cantChangeSlider">
              <IconButton
                disabled={item.achieved}
                bg="var(--chakra-colors-green-300)"
                color="white.500"
                onClick={handleEditClick}
                aria-label="editar"
                className="cantChangeSlider"
                size="md"
                fontSize="1.5rem"
                icon={<GiCheckMark />}
                borderRadius="50%"
              />
            </Box>
          )}
        </CardList>
      </Box>
    );
  } else {
    return (
      <DTCard
        mt="1rem"
        className={!!className && className}
        p="1rem 1.5rem"
        bg="white.500"
        display="flex"
        w={location.pathname === "/dashboard" ? "16rem" : "65%"}
        borderRadius="4px"
        groupcolor={categoryCollor}
      >
        {" "}
        <CardSlider
          className="cantChangeSlider"
          opencheck={(onlyRead() === false && cardOpen).toString()}
          category={item.category}
          groupcolor={categoryCollor}
        ></CardSlider>
        <Text as="h2" noOfLines={1}>
          {item.title}
        </Text>
        <Badge
          className="category"
          category={gpDashboard ? gpDashboard.category : item.category}
          mt="8px"
        >
          {!!item.category && item.category.split("@Hero:")[1]}
          {!!gpDashboard && gpDashboard.category.split("@Hero:")[1]}
        </Badge>
        {type === editTypes["TYPE_HABIT"] && (
          <Box
            display="flex"
            alignItems="center"
            justifyContent="space-around"
            mt="1rem"
          >
            <CircularProgress
              value={item.how_much_achieved}
              size="7.5rem"
              color="green.300"
            >
              <CircularProgressLabel fontWeight="bold" color="gray.500">
                {item.how_much_achieved}%
              </CircularProgressLabel>
            </CircularProgress>
            <Box>
              <Box bg="gray.300" borderRadius="50%" mb="1rem">
                <DeleteHabitModal
                  type={type}
                  item={item}
                  userHabitsList={userHabitsList}
                  setUserHabitsList={setUserHabitsList}
                  group={group}
                  setGroup={setGroup}
                  setRefresh={setRefresh}
                  setCardOpen={setCardOpen}
                ></DeleteHabitModal>
              </Box>
              <IconButton
                bg="var(--chakra-colors-green-300)"
                color="white.500"
                onClick={handleEditClick}
                aria-label="editar"
                size="md"
                fontSize="1.5rem"
                icon={<GiCheckMark />}
                borderRadius="50%"
              />
            </Box>
          </Box>
        )}
        {type === editTypes["TYPE_ACTIVITY"] &&
          diffDate(item.realization_time) >= 0 && (
            <Flex justifyContent="space-between" display="flex">
              {diffDate(item.realization_time) > 1 ? (
                <Flex
                  direction="row"
                  align="center"
                  justifyContent="space-between"
mt="0.5rem"
                >
                  <Text
                    as="p"
                    fontSize="1rem"
                    className="title"
                    color="#414141"
                  >
                    Prazo: faltam
                  </Text>
                  <Flex
                    bg="var(--chakra-colors-green-300)"
                    color="white.500"
                    w="2.5rem"
                    h="2.5rem"
                    fontSize="1.2rem"
                    fontWeight="bold"
                    align="center"
                    justify="center"
                    borderRadius="50%"
                    m="0 0.3rem"
                  >
                    {diffDate(item.realization_time)}
                  </Flex>
                  <Text
                    as="p"
                    className="title"
                    fontSize="1rem"
                    color="#414141"
                  >
                    dias.
                  </Text>
                </Flex>
              ) : (
                <Flex direction="row" align="center">
                  <Text
                    as="p"
                    fontSize="1rem"
                    className="title"
                    color="#414141"
mt="0.5rem"
                  >
                    Prazo: falta
                  </Text>
                  <Flex
                    bg="var(--chakra-colors-red-300)"
                    color="white.500"
                    w="2.5rem"
                    h="2.5rem"
                    fontSize="1.2rem"
                    fontWeight="bold"
                    align="center"
                    justify="center"
                    borderRadius="50%"
                    m="0 0.3rem"
                  >
                    {diffDate(item.realization_time)}
                  </Flex>
                  <Text
                    as="p"
                    className="title"
                    fontSize="1rem"
                    color="#414141"
                  >
                    dia.
                  </Text>
                </Flex>
              )}
              {onlyRead() === false && (
                <Box className="cantChangeSlider">
                  <IconButton
                    disabled={item.achieved}
                    bg="var(--chakra-colors-green-300)"
                    color="white.500"
                    onClick={handleEditClick}
                    aria-label="editar"
                    className="cantChangeSlider"
                    size="md"
                    fontSize="1.5rem"
                    icon={<GiCheckMark />}
                    borderRadius="50%"
                  />
                </Box>
              )}
            </Flex>
          )}
        {type === editTypes["TYPE_ACTIVITY"] &&
          diffDate(item.realization_time) < 0 && (
            <Flex direction="row" align="center" justifyContent="space-between">
              <Flex direction="row" align="center" mt="0.5rem">
                <Text>Prazo: </Text>
                <Flex
                  bg="var(--chakra-colors-red-500)"
                  color="white.500"
                  h="1.8rem"
                  fontWeight="bold"
                  align="center"
                  justifyContent="center"
                  borderRadius="5%"
                  fontSize="1rem"
                  p="0.4rem"
                  ml="0.2rem"
                >
                  expirou
                </Flex>
              </Flex>
              {onlyRead() === false && (
                <Box className="cantChangeSlider">
                  <IconButton
                    disabled={item.achieved}
                    bg="var(--chakra-colors-green-300)"
                    color="white.500"
                    onClick={handleEditClick}
                    aria-label="editar"
                    className="cantChangeSlider"
                    size="md"
                    fontSize="1.5rem"
                    icon={<GiCheckMark />}
                    borderRadius="50%"
                  />
                </Box>
              )}
            </Flex>
          )}
      </DTCard>
    );
  }
};
export default Card;
