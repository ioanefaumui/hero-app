import { Box } from "@chakra-ui/layout";
import { Progress } from "@chakra-ui/progress";
import styled from "styled-components";
import { categoriesColors } from "../../utils";

export const CardList = styled.li`
  position: relative;
  background: var(--chakra-colors-white-500);
  list-style: none;
  padding: 0.5rem 1rem 0.5rem 1.7rem;
  border-radius: 0.25rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
  overflow: hidden;
  margin-top: 1rem;

  p {
    font-size: 0.8rem;
  }

  h2 {
    color: #414141;
    font-family: "Poppins", sans-serif;
    font-size: 1rem;
    font-weight: 700;
    text-align: left;
  }

  span {
    background: ${({ category, groupcolor }) => {
      return (
        groupcolor ||
        categoriesColors[category] ||
        categoriesColors["@Hero:Outra"]
      );
    }};
    color: #fff;
  }
`;

export const CardSlider = styled.div`
  z-index: 1;
  display: grid;
  place-items: center;
  padding: 0.5rem;
  overflow: hidden;
  min-width: 80px;
  height: 100%;
  transition: ease-in-out 300ms;
  transform: ${({ opencheck }) =>
    opencheck === "true" ? "translateX(0)" : "translateX(-64px)"};
  position: absolute;
  left: 0;
  top: 0;
  background: ${({ category, groupcolor }) => {
    return (
      groupcolor ||
      categoriesColors[category] ||
      categoriesColors["@Hero:Outra"]
    );
  }};
  @media only screen and (min-width: 900px) {
    min-width: 30px;
    width: 30px;
    left: 45px;
    border-top-left-radius: 8px;
    border-bottom-left-radius: 8px;
  }
`;

export const AchieveBar = styled(Progress)`
  position: static;

  div {
    background: var(--chakra-colors-green-300);
  }
  width: ${({ opencheck }) =>
    opencheck === "true" ? "calc(100% - 64px)" : "100%"};
  transition: ease-in-out 300ms;
`;

export const DTCard = styled(Box)`
  border: 1px solid #e8e8e8;
  flex-direction: column;
  transition: cubic-bezier(0.23, 1, 0.32, 1) 700ms;
  cursor: pointer;
  position: relative;

  .svg {
    font-size: 5rem;
    color: #414141;
  }
  h2 {
    font-size: 1rem;
    font-weight: bold;
    color: #414141;
  }

  & > span {
    width: fit-content;
    color: var(--chakra-colors-white-500);
  }
  .category {
    background: ${({ category, groupcolor }) => {
      return (
        groupcolor ||
        categoriesColors[category] ||
        categoriesColors["@Hero:Outra"]
      );
    }};
  }

  :hover {
    box-shadow: 2px 7px 14px -8px rgb(0 0 0);
    transform: scale(1.07);
  }
`;

export const StyledBox = styled(Box)`
  transform: ${({ opencheck }) =>
    opencheck === "true" ? "translateX(64px)" : "translateX(0)"};
  transition: ease-in-out 300ms;
  .title {
    max-width: ${({ opencheck }) => (opencheck === "true" ? "65%" : "none")};
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
`;
