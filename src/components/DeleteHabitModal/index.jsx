import {
  AlertDialog,
  AlertDialogBody,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogContent,
  AlertDialogOverlay,
} from "@chakra-ui/modal";
import { Button, IconButton } from "@chakra-ui/button";
import { useState } from "react";
import api from "../../services/api";
import { useToast } from "@chakra-ui/toast";
import { Box } from "@chakra-ui/layout";
import { CheckCircleIcon, DeleteIcon, WarningIcon } from "@chakra-ui/icons";
import { editTypes, toastConfig } from "../../utils";
import { useUser } from "../../providers/user";
import { useUserGroups } from "../../providers/groups";

const DeleteHabitModal = ({
  item,
  type,
  userHabitsList,
  setUserHabitsList,
  setCardOpen,
  group,
  setGroup,
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const toast = useToast();
  const {
    user: { token },
  } = useUser();
  const { userGroupsList, setUserGroupsList } = useUserGroups();

  const heroAlert = (msg, isSuccess = true) => {
    const {
      DURATION,
      POSITION,
      COLOR,
      PADDING,
      DISPLAY,
      ALIGN_ITEMS,
      MARGIN_RIGHT,
      GREEN,
      RED,
    } = toastConfig;
    if (isSuccess) {
      toast({
        position: POSITION,
        duration: DURATION,
        render: () => (
          <Box
            color={COLOR}
            p={PADDING}
            display={DISPLAY}
            alignItems={ALIGN_ITEMS}
            bg={GREEN}
          >
            <CheckCircleIcon mr={MARGIN_RIGHT} />
            {msg}
          </Box>
        ),
      });
    } else {
      toast({
        position: POSITION,
        duration: DURATION,
        render: () => (
          <Box
            color={COLOR}
            p={PADDING}
            display={DISPLAY}
            alignItems={ALIGN_ITEMS}
            bg={RED}
          >
            <WarningIcon mr={MARGIN_RIGHT} />
            {msg}
          </Box>
        ),
      });
    }
  };

  const onClose = () => {
    const { TYPE_ACTIVITY, TYPE_GOAL, TYPE_HABIT } = editTypes;
    const { id, title } = item;
    let errMsg = `Erro ao tentar excluir. Por favor, `;

    switch (type) {
      case TYPE_ACTIVITY:
        api
          .delete(`/activities/${id}/`, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
          .then((response) => {
            const newGoals = group.activities.filter(
              (activity) => activity.id !== id
            );
            setGroup({ ...group, activities: newGoals });
            const newGPList = userGroupsList.map((gp) => {
              if (gp.id === group.id) {
                return {
                  ...gp,
                  activities: newGoals,
                };
              } else {
                return gp;
              }
            });
            setUserGroupsList(newGPList);

            heroAlert(`Atividade "${title}" excluída com sucesso.`);
          })
          .catch((error) => {
            if (error.response?.status === 401) {
              errMsg += "refaça seu login.";
            } else {
              errMsg += "tente mais tarde.";
            }
            heroAlert(errMsg, false);
            console.dir(error);
          });
        break;
      case TYPE_GOAL:
        api
          .delete(`/goals/${id}/`, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
          .then((response) => {
            const newGoals = group.goals.filter((goal) => goal.id !== id);
            setGroup({ ...group, goals: newGoals });
            const newGPList = userGroupsList.map((gp) => {
              if (gp.id === group.id) {
                return {
                  ...gp,
                  goals: newGoals,
                };
              } else {
                return gp;
              }
            });
            setUserGroupsList(newGPList);
            heroAlert(`Meta "${title}" excluída com sucesso.`);
          })
          .catch((error) => {
            if (error.response.status === 401) {
              errMsg += "refaça seu login.";
            } else {
              errMsg += "tente mais tarde.";
            }
            heroAlert(errMsg, false);
            console.dir(error);
          });

        break;
      case TYPE_HABIT:
        api
          .delete(`/habits/${id}/`, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
          .then(() => {
            const filteredList = userHabitsList.filter(
              ({ id }) => item.id !== id
            );

            setUserHabitsList(filteredList);
            heroAlert(`Hábito "${title}" excluído com sucesso.`);
          })
          .catch((error) => {
            if (error.response.status === 401) {
              errMsg += "refaça seu login.";
            } else {
              errMsg += "tente mais tarde.";
            }
            heroAlert(errMsg, false);
            console.dir(error);
          });
        break;
      default:
        return;
    }
    setIsOpen(false);
  };

  return (
    <>
      <IconButton
        variant="none"
        color="white.500"
        onClick={() => setIsOpen(true)}
        icon={<DeleteIcon fontSize="1.2rem" />}
        fontSize="1.5rem"
        borderRadius="50%"
        position="static"
      />

      <AlertDialog isOpen={isOpen} onClose={onClose}>
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Excluir
            </AlertDialogHeader>

            {item && (
              <AlertDialogBody>
                Tem certeza que gostaria de excluir "{item.title}"?
              </AlertDialogBody>
            )}

            <AlertDialogFooter justifyContent="space-between">
              <Button
                colorScheme="green"
                onClick={() => {
                  setIsOpen(false);
                  setCardOpen(false);
                }}
                textTransform="uppercase"
              >
                voltar
              </Button>
              <Button
                colorScheme="green"
                variant="outline"
                onClick={onClose}
                textTransform="uppercase"
              >
                excluir
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
    </>
  );
};

export default DeleteHabitModal;
