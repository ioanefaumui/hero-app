import { StyledFooter } from "./styles";
import { Flex } from "@chakra-ui/react";
import { ChevronUpIcon } from "@chakra-ui/icons";
import { useMenus } from "../../providers/menus";
import { MenuCover } from "../NavMenu/styles";

const FooterComponent = ({ children = undefined, childrenSize = 0 }) => {
  const { toggleBottomMenu, bottomMenuOpened } = useMenus();

  return (
    <>
      <StyledFooter
        className={bottomMenuOpened === true ? "openned" : ""}
        childrenSize={childrenSize}
      >
        <Flex justifyContent="center">
          <button
            className={bottomMenuOpened === true ? "openned" : ""}
            onClick={(e) => toggleBottomMenu()}
          >
            <span>
              <ChevronUpIcon />
            </span>
          </button>
        </Flex>
        {!!children && children}
      </StyledFooter>
      <MenuCover isopen={bottomMenuOpened.toString()}></MenuCover>
    </>
  );
};

export default FooterComponent;
