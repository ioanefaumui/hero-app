import styled from 'styled-components';


export const StyledFooter = styled.footer`

background: #319795;
z-index: 98;
position: fixed;
bottom: 0;
left: 0;
min-height: 45px;
width: 100%;
transform: ${ props => 'translateY(' +(props.childrenSize) + 'px)' };
padding-top: 40px;
transition: transform .4s;

&.openned{
    transform: translateY(0);
    transition: transform  .4s;
}

button.openned{
    transform: rotate(180deg);
    transition: transform  .4s;
}

& > div > button {
    color: #fff;
    font-size: 1.5rem;
    border-radius: 50%;
    width: 4.375rem;
    height: 4.375rem;
    background: #319795;
    position: absolute;
    top: -1.5rem;
    display: flex;
    align-items: center;
    justify-content: center;
    transform: rotate(0);
    transition: transform  .7s;

    span{
       border-radius: 50%;
       background: #fff;
       width: 2.5rem;
       height: 2.5rem;
       color: #319795;
       display: flex;
       align-items: center;
       justify-content: center;
     }
 }


`;

