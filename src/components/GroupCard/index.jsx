import { StyledContainer, Content, ContentCategory, ContentButton } from "./styles";
import api from "../../services/api.js";
import { Badge, Text, useToast } from "@chakra-ui/react";
import { CheckCircleIcon, WarningIcon } from "@chakra-ui/icons";
import { Box, Flex } from "@chakra-ui/react";
import { useUser } from "../../providers/user";
import { useHistory, useLocation } from "react-router-dom";
import { categoriesColors } from "../../utils/";
import crownImage from "../../assets/img/crown.svg";
import { DTCard } from "../Card/styles";
import { SCREEN_BREAKPOINT } from "../../utils";
import { useWindow } from "../../providers/window";

const GroupCardComponent = ({ group, loadGroups }) => {
  const history = useHistory();
  const location = useLocation();
  const toast = useToast();
  const {
    user: { token, id },
  } = useUser();

  const goToGroupPage = (e) => {
    if (
      !e.target.className.includes("subscribe") &&
      !e.target.className.includes("unsubscribe")
    ){
      history.push(`${location.pathname}/groups/${group.id}`);
    }
  };

  const subscribe = () => {
    api
      .post(
        `/groups/${group.id}/subscribe/`,
        {},
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      )
      .then(() => {
        loadGroups();
        toast({
          position: "top-right",
          duration: 1500,
          render: () => (
            <Box
              color="white.500"
              p={4}
              display="flex"
              alignItems="center"
              bg="green.300"
            >
              <CheckCircleIcon mr={2.5} />
              Inscrição realizada com sucesso.
            </Box>
          ),
        });
      })
      .catch((e) => {
        toast({
          position: "top-right",
          duration: 1500,
          render: () => (
            <Box
              color="white.500"
              p={4}
              display="flex"
              alignItems="center"
              bg="red.300"
            >
              <WarningIcon mr={2.5} />
              Não foi possível se inscrever neste grupo.
            </Box>
          ),
        });
      });
  };

  const unsubscribe = () => {
    api
      .delete(`/groups/${group.id}/unsubscribe/`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(() => {
        loadGroups();
        toast({
          position: "top-right",
          duration: 1500,
          render: () => (
            <Box
              color="white.500"
              p={4}
              display="flex"
              alignItems="center"
              bg="green.300"
            >
              <CheckCircleIcon mr={2.5} />
              Exclusão realizada com sucesso.
            </Box>
          ),
        });
      })
      .catch((e) => {
        toast({
          position: "top-right",
          duration: 1500,
          render: () => (
            <Box
              color="white.500"
              p={4}
              display="flex"
              alignItems="center"
              bg="red.300"
            >
              <WarningIcon mr={2.5} />
              Não foi possível realizar a exclusão do grupo.
            </Box>
          ),
        });
      });
  };

  const { pageWidth } = useWindow();

  if (pageWidth < SCREEN_BREAKPOINT) {
    return (
      <StyledContainer
        bg="white.500"
        onClick={goToGroupPage}
        borderColor={categoriesColors[group.category]}
      >
        {id === group.creator.id ? <img src={crownImage} alt="creator" /> : ""}
        <Box className="border"></Box>
        <h1>{group.name}</h1>
        <Box>
          <p>{group.description}</p>
          <span className="memberNumber">
            <span>{group.users_on_group.length}</span>
            {group.users_on_group.length > 1 ? "membros" : "membro"}
          </span>
        </Box>
        <Box>
          <span className="category">{group.category.split("@Hero:")[1]}</span>
          {group.users_on_group.find((item) => item.id === id) === undefined &&
          group.creator.id !== id ? (
            <button onClick={subscribe} className="subscribe">
              Participar
            </button>
          ) : group.users_on_group.find((item) => item.id === id) !==
              undefined && group.creator.id !== id ? (
            <button onClick={unsubscribe} className="unsubscribe">
              Sair do grupo
            </button>
          ) : (
            ""
          )}
        </Box>
      </StyledContainer>
    );
  } else {
    return (
      <DTCard
        onClick={goToGroupPage}
        w="16rem"
        h="13rem"
        pos="relative"
        bg="white.500"
        display="flex"
        p="1rem 1.5rem"
        borderRadius="4px"
        flexDirection="column"
        justifyContent="space-between"
        boxShadow="0px 5px 20px -7px rgba(0, 0, 0, 0.25)"
      >
        <Box>
          <Box pos="absolute" right="1rem">
            {id === group.creator.id ? (
              <img src={crownImage} alt="creator" />
            ) : (
              ""
            )}
          </Box>
          <Text as="h2" noOfLines={1}>
            {group.name}
          </Text>
          <Box >
            <Text
              mt="0.5rem"
              mb="0.5rem"
              lineHeight="1.2"
              fontWeight="600"
              noOfLines={4}
              color="gray.500"
            >
              {group.description}
            </Text>
          </Box>
        </Box>
        <Content>      
          <ContentCategory>      
            <Badge
              category={group.category}
              padding=".3rem .3rem .2rem .3rem"
              bg={categoriesColors[group.category]}
              borderRadius="2px"
            >
              {!!group.category && group.category.split("@Hero:")[1]}
            </Badge>
            <Flex justify="center">      
              <Text mr="0.5rem" fontWeight="bold">
                {group.users_on_group.length}          
              </Text>
              <span className="memberNumber">
                {group.users_on_group.length > 1 ? " membros" : " membro"}          
              </span>
            </Flex>
          </ContentCategory>

          <ContentButton>
            {group.users_on_group.find((item) => item.id === id) === undefined &&
            group.creator.id !== id ? (
              <button onClick={subscribe} className="subscribe">
                Participar
              </button>
            ) : group.users_on_group.find((item) => item.id === id) !==
                undefined && group.creator.id !== id ? (
              <button onClick={unsubscribe} className="unsubscribe">
                Sair do grupo
              </button>
            ) : (
              ""
            )}
          </ContentButton>
        </Content>
      </DTCard>
    );
  }
};

export default GroupCardComponent;
