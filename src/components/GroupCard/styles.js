import styled from "styled-components";

export const StyledContainer = styled.div`

  margin: 1rem 0;
  cursor: pointer;
  padding: 1rem 1rem .1rem 2rem;
  position: relative;
  border-radius: 10px;
  width: 85%;
  margin: 0 auto;
  margin-top: 1rem;
  transform: scale(1);
  box-shadow: none;
  transition: transform .3s;
  box-shadow: 1px 1px 8px -2px #000;

  :hover{
    transform: scale(1.1);
  }

  .border{
    height: 100%;
    width: 1rem;
    position: absolute;
    background: ${ props => props.borderColor ? props.borderColor : 'rgba(173,83,83,0.6)'};
    top: 0;
    left: 0;
    border-top-left-radius: 10px;
    border-bottom-left-radius: 10px;
  }

  h1{
    margin-bottom: .5rem;
    font-size: 1.15rem;
    width: 100%;
    max-width: 15rem;
    white-space : nowrap;
    overflow : hidden;
    text-overflow: ellipsis;
  }
  
  > div:not(.border){
    display: flex;
    justify-content: space-between;
    margin-bottom: 1rem;
  }
  > div + div{
    margin: 0;
  }
  p{
    font-size: .75rem;
    font-weight: bold;
    color: #777;
    margin-right: .5rem;
    font-family: 'Nunito', sans-serif;
    width: 100%;
    max-width: 15rem;
    white-space : nowrap;
    overflow : hidden;
    text-overflow: ellipsis;
  }

  .memberNumber{
    font-family: 'Nunito', sans-serif;
    font-size: 12px;
    color: #000;
    font-weight: bold;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;
  }
  .memberNumber span{
    font-size: 32px;
  }

  .category{
    background: ${ props => props.borderColor ? props.borderColor : 'rgba(173,83,83,0.6)'};
    padding: .3rem .3rem .2rem .3rem;
    color: #fff;
    font-weight: 400;
    font-size: .7rem;
    border-radius: 2px;
    text-transform: uppercase;
    margin: 0;
  }

  .subscribe{
    color: #fff;
    font-size: .7rem;
    padding: .3rem .3rem .2rem .3rem;
    background: #4EC685;
    border-radius: 4px;
    text-transform: uppercase;
    position: relative;
    z-index: 2;
    transform: scale(1);
    transition: transform .4s;
  }

  .unsubscribe{
    color: #fff;
    font-size: .7rem;
    padding: .3rem .3rem .2rem .3rem;
    background: #319795;
    border-radius: 4px;
    text-transform: uppercase;
    position: relative;
    z-index: 2;
    transform: scale(1);
    transition: transform .4s;
  }

  .subscribe:hover, .unsubscribe:hover{
    transform: scale(1.3);
    box-shadow: 0px 4px 9px -3px #000;
  }

`;

export const Content = styled.div`
  height: 3.5rem;
`;

export const ContentCategory = styled.div`
  margin: 0 0 0.5rem 0;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 0.3rem 0;

  .memberNumber {
    font-family: 'Nunito', sans-serif;
    font-size: 0.75rem;
    color: #000;
    font-weight: bold;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

export const ContentButton = styled.div`
  margin: 0.5rem 0;

  .subscribe{
    color: #fafcff;
    font-size: .7rem;
    padding: .35rem .3rem .35rem .3rem;
    background: #5cb85c;
    border-radius: 4px;
    text-transform: uppercase;
    position: relative;
    z-index: 2;
    transform: scale(1);
    transition: transform .4s;
  }

  .unsubscribe{
    color: #fafcff;
    font-size: .7rem;
    padding: .35rem .3rem .35rem .3rem;
    background: #c4c4c4;
    border-radius: 4px;
    text-transform: uppercase;
    position: relative;
    z-index: 2;
    transform: scale(1);
    transition: transform .4s;
  }

  .subscribe:hover, .unsubscribe:hover{
    transform: scale(1.1);
    box-shadow: 0px 4px 9px -3px #000;
  }
`;
