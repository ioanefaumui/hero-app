import { Button, IconButton } from "@chakra-ui/button";
import { ChevronDownIcon } from "@chakra-ui/icons";
import { Flex } from "@chakra-ui/layout";
import { Link, NavLink, useHistory } from "react-router-dom";
import {
  HeroNav,
  MenuCover,
  MenuWrapper,
  NavMenuItem,
  DesktopMenu,
} from "./styles";
import logoWhite from "../../assets/img/logo-white.svg";
import { useUser } from "../../providers/user";
import { useMenus } from "../../providers/menus";
import { Image } from "@chakra-ui/image";
import whiteLogo from "../../assets/img/logo-white.svg";
import { SCREEN_BREAKPOINT } from "../../utils";
import { useWindow } from "../../providers/window";
import { useUserGroups } from "../../providers/groups";

const NavMenu = () => {
  const { setUser } = useUser();
  const {
    toggleTopMenu,
    topMenuOpened,
    setBottomMenuOpened,
    setTopMenuOpened,
  } = useMenus();
  const history = useHistory();
  const { setUserGroupsList } = useUserGroups();

  const handleSession = () => {
    setUser({
      token: "",
      id: undefined,
      auth: false,
    });
    setBottomMenuOpened(false);
    setTopMenuOpened(false);
    localStorage.clear();
    setUserGroupsList([]);
    history.push("/");
  };

  const { pageWidth } = useWindow();

  if (pageWidth < SCREEN_BREAKPOINT) {
    return (
      <header>
        <Flex
          position="fixed"
          width="100%"
          top="0"
          left="0"
          zIndex="2"
          bg="green.500"
          p="20px"
          justify="space-between"
          align="center"
        >
          <Link to="/">
            <img src={logoWhite} alt="hero" />
          </Link>
          <IconButton
            onClick={toggleTopMenu}
            icon={<ChevronDownIcon />}
            color="green.500"
            borderRadius="50%"
            fontSize="1.5rem"
            style={topMenuOpened ? { transform: "rotate(180deg)" } : null}
          />
        </Flex>
        <HeroNav>
          <MenuWrapper bg="green.500" isopen={topMenuOpened.toString()}>
            <NavMenuItem to="/dashboard" onClick={toggleTopMenu}>
              Meus hábitos
            </NavMenuItem>
            <NavMenuItem to="/mygroups" onClick={toggleTopMenu}>
              Meus grupos
            </NavMenuItem>
            <NavMenuItem to="/searchGroups" onClick={toggleTopMenu}>
              Procurar grupo
            </NavMenuItem>
            <Button
              onClick={handleSession}
              colorScheme="none"
              textDecor="underline"
              mt="16px"
            >
              Sair
            </Button>
          </MenuWrapper>
        </HeroNav>
        <MenuCover isopen={topMenuOpened.toString()}></MenuCover>
      </header>
    );
  } else {
    return (
      <DesktopMenu w="auto" h="100%" bg="green.500" pt="1rem" pos="relative">
        <Image src={whiteLogo} ml="1rem" />
        <nav>
          <NavLink
            to="/dashboard"
            activeStyle={{
              color: "var(--chakra-colors-green-500)",
              background: "var(--chakra-colors-gray-100)",
            }}
          >
            Dashboard
          </NavLink>
          <NavLink
            to="/mygroups"
            activeStyle={{
              color: "var(--chakra-colors-green-500)",
              background: "var(--chakra-colors-gray-100)",
            }}
          >
            Meus grupos
          </NavLink>
          <NavLink
            to="/searchGroups"
            activeStyle={{
              color: "var(--chakra-colors-green-500)",
              background: "var(--chakra-colors-gray-100)",
            }}
          >
            Procurar grupo
          </NavLink>

          <button onClick={handleSession}>Sair</button>
        </nav>
      </DesktopMenu>
    );
  }
};

export default NavMenu;
