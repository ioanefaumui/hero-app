import { Box, Center } from "@chakra-ui/layout";
import { Link } from "react-router-dom";
import styled from "styled-components";

export const HeroNav = styled.nav`
  padding: 8px;
  display: flex;
  justify-content: center;
`;

export const MenuWrapper = styled(Center)`
  height: ${({ isopen }) => (isopen === "true" ? "calc(100% - 70%)" : "0")};
  overflow: hidden;
  transition: ease 200ms;
  color: #fafcff;
  flex-direction: column;
  position: fixed;
  z-index: 99;
  width: calc(100% - 16px);
`;

export const NavMenuItem = styled(Link)`
  font-family: "Poppins", sans-serif;
  font: 700;
  width: 100%;
  text-align: center;
  padding: 8px;

  :hover {
    transition: ease 200ms;
    background-color: #fafcff;
    color: #319795;
  }
`;

export const MenuCover = styled.div`
  height: ${({ isopen }) => (isopen === "true" ? "100%" : "0")};
  width: 100%;
  position: absolute;
  display: flex;
  opacity: ${({ isopen }) => (isopen === "true" ? "1" : "0")};
  transition: opacity ease 400ms;
  top: 0;
  z-index: 1;
  background-color: rgba(0, 0, 0, 0.7);
`;

export const DesktopMenu = styled(Box)`
  nav {
    margin-top: 4.5rem;
    display: flex;
    flex-direction: column;
    text-indent: 1rem;
    white-space: nowrap;

    > a {
      color: var(--chakra-colors-white-500);
      font-family: "Poppins", sans-serif;
      font-weight: 700;
      padding: 1rem 1rem 1rem 0;
      font-size: 1.125rem;

      :hover {
        text-decoration: none;
        color: var(--chakra-colors-green-500);
        background: var(--chakra-colors-gray-100);
      }
    }

    button {
      color: var(--chakra-colors-white-500);
      width: min-content;
      padding: 0.5rem 1rem;
      position: absolute;
      bottom: 1rem;
      left: 50%;
      transform: translateX(-50%);
    }
  }

  .sub-heading {
    display: block;
    font-weight: bold;
    color: #bdc0c4;
    margin-top: 1rem;
  }
`;
