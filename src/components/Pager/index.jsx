import { ChevronLeftIcon, ChevronRightIcon } from "@chakra-ui/icons";
import { HStack, IconButton, Text } from "@chakra-ui/react";
import { useWindow } from "../../providers/window";
import { SCREEN_BREAKPOINT } from "../../utils";

/**
 * Represents a pager component.
 * @param {Object} pages, the object as { prev: null, nex: null, current: 1 } to guide the component.
 * @param {function} handlePagesClick(page), where "page" is the flag that will guide the function.
 */
const Pager = ({ pages, handlePagesClick }) => {
	const { pageWidth } = useWindow();

	if (pageWidth < SCREEN_BREAKPOINT) {
		//MOBILE VERSION:
		return (
			<HStack justifyContent="center" mt="0.5rem">
				<IconButton
					icon={<ChevronLeftIcon color="green.500" />}
					disabled={!pages.prev ? true : false}
					borderRadius="50%"
					fontSize="1.5rem"
					size="sm"
					onClick={() => handlePagesClick("prev")}
				/>
				<Text color="green.500" fontWeight="700">
					{pages.current}
				</Text>
				<IconButton
					icon={<ChevronRightIcon color="green.500" />}
					disabled={!pages.next ? true : false}
					borderRadius="50%"
					fontSize="1.5rem"
					size="sm"
					onClick={() => handlePagesClick("next")}
				/>
			</HStack>
		);
	} else {
		//DESKTOP VERSION:
		return (
			<HStack justifyContent="center" mb="1rem">
				<IconButton bg="white"
					icon={<ChevronLeftIcon color="green.500" />}
					disabled={!pages.prev ? true : false}
					borderRadius="50%"
					fontSize="2rem"
					size="sm"
					onClick={() => handlePagesClick("prev")}
				/>
				<Text color="green.500" fontWeight="700" fontSize="1.1rem">
					{pages.current}
				</Text>
				<IconButton bg="white"
					icon={<ChevronRightIcon color="green.500"/>}
					disabled={!pages.next ? true : false}
					borderRadius="50%"
					fontSize="2rem"
					size="sm"
					onClick={() => handlePagesClick("next")}
				/>
			</HStack>
		);
	}
};

export default Pager;
