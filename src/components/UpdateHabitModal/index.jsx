import { IconButton, Button } from "@chakra-ui/button";
import { Checkbox } from "@chakra-ui/checkbox";
import { useDisclosure } from "@chakra-ui/hooks";
import { HStack, Box, Text } from "@chakra-ui/layout";
import {
  ModalFooter,
  ModalBody,
  ModalHeader,
  ModalContent,
  ModalOverlay,
  Modal,
} from "@chakra-ui/modal";
import {
  SliderFilledTrack,
  SliderThumb,
  SliderTrack,
  Slider,
} from "@chakra-ui/slider";
import { TagLabel } from "@chakra-ui/tag";
import { useEffect, useRef, useState } from "react";
import { GiCheckMark } from "react-icons/gi";
import { BiEditAlt } from "react-icons/bi";
import api from "../../services/api";

const UpdateHabitModal = ({ habit }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const fromRef = useRef();
  const [howMuchAchieved, setHowMuchAchieved] = useState(0);
  const [isAchieved, setIsAchieved] = useState(false);

  useEffect(() => {}, [howMuchAchieved]);

  const handleChange = (value) => {
    setHowMuchAchieved(value);
    if (value === 100) {
      setIsAchieved(true);
    } else if (isAchieved) {
      setIsAchieved(false);
    }
  };

  api
    .patch(`/habits/${habit?.id}`, {
      how_much_achieved: howMuchAchieved,
      achieved: isAchieved,
    })
    .catch((error) => console.error(error));

  return (
    <>
      <IconButton
        colorScheme="green"
        onClick={onOpen}
        ref={fromRef}
        aria-label="editar"
        size="md"
        fontSize="1.5rem"
        icon={<BiEditAlt />}
      />

      <Modal
        isOpen={isOpen}
        onClose={onClose}
        finalFocusRef={fromRef}
        closeOnOverlayClick={false}
        isCentered
        motionPreset="slideInBottom"
        size="lg"
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Editar</ModalHeader>
          <Text>{habit?.title}</Text>
          <ModalBody>
            <HStack width="100%" justifyContent="space-between">
              <Slider
                defaultValue={0}
                min={0}
                max={100}
                step={10}
                width="86%"
                onChange={(value) => handleChange(value)}
              >
                <SliderTrack bg="gray.500">
                  <Box position="relative" right={10} />
                  <SliderFilledTrack bg="green.500" />
                </SliderTrack>
                <SliderThumb boxSize={8}>
                  <TagLabel color="green.500">{howMuchAchieved}</TagLabel>
                </SliderThumb>
              </Slider>
              <TagLabel color="green.500">%</TagLabel>
            </HStack>
            <Checkbox
              colorScheme="green"
              size="lg"
              icon={<GiCheckMark />}
              mt={4}
              isChecked={isAchieved}
            >
              Concluído
            </Checkbox>
          </ModalBody>

          <ModalFooter justifyContent="start">
            <Button
              colorScheme="green"
              color="white.500"
              onClick={onClose}
              textTransform="uppercase"
            >
              voltar
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default UpdateHabitModal;
