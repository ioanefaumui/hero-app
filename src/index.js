import { ChakraProvider } from "@chakra-ui/react";
import { StrictMode } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import App from "./App";
import heroTheme, { GlobalStyles } from "./styles/GlobalStyles";
import Providers from './providers';

ReactDOM.render(
	<StrictMode>
		<GlobalStyles />
		<Providers>
			<ChakraProvider resetCSS theme={heroTheme}>
				<BrowserRouter>
					<App />
				</BrowserRouter>
			</ChakraProvider>
		</Providers>
	</StrictMode>,
	document.getElementById("root"),
);
