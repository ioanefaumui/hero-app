import NavMenu from "../../components/NavMenu";
import { Container, ContainerInfo } from "./styles";
import Card from "../../components/Card";
import AddHabit from "../../components/AddHabit";
import { useEffect, useState } from "react";
import api from "../../services/api";
import { editTypes, toastConfig, TAG_USER_HABITS_LIST } from "../../utils";
import {
  Box,
  Flex,
  Heading,
  SimpleGrid,
  useToast,
  Text,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
  Button,
} from "@chakra-ui/react";
import { AddIcon, WarningIcon } from "@chakra-ui/icons";
import Footer from "../../components/FooterMenu";
import gifLoading from "../../assets/img/loading.gif";
import { useUser } from "../../providers/user";
import { useUserGroups } from "../../providers/groups";
import { SCREEN_BREAKPOINT, USER_TAG } from "../../utils";
import { useWindow } from "../../providers/window";
import { Redirect } from "react-router-dom";

const Dashboard = () => {
  const {
    user: { token, auth },
    setUser,
  } = useUser();

  const {
    setUserGoalsList,
    setUserActivitiesList,
    userGroupsList,
    userGoalsList,
    userActivitiesList,
  } = useUserGroups();
  const [ownerList, setOwnerList] = useState([]);
  const toast = useToast();
  const [userHabitsList, setUserHabitsList] = useState(
    JSON.parse(localStorage.getItem(TAG_USER_HABITS_LIST)) || []
  );

  const [isLoading, setIsLoading] = useState(true);
  const [isToRender, setIsToRender] = useState(false);
  const { TYPE_HABIT, TYPE_GOAL, TYPE_ACTIVITY } = editTypes;

  const { isOpen, onOpen, onClose } = useDisclosure();

  const {
    DURATION,
    POSITION,
    COLOR,
    PADDING,
    DISPLAY,
    ALIGN_ITEMS,
    MARGIN_RIGHT,
    RED,
  } = toastConfig;

  useEffect(() => {
    if (userGroupsList.length > 0) {
      let aux = userGroupsList.map((group) => {
        return {
          ownerId: group.creator.id,
          groupId: group.id,
          category: group.category,
        };
      });
      setOwnerList(aux);
    }
  }, [userGroupsList]);

  useEffect(() => {
    localStorage.setItem(TAG_USER_HABITS_LIST, JSON.stringify(userHabitsList));

    if (userHabitsList.length === 0 || isToRender) {
      api
        .get("/habits/personal/", {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((response) => {
          const { data } = response;
          setUserHabitsList(data);

          localStorage.setItem(TAG_USER_HABITS_LIST, JSON.stringify(data));
        })
        .catch(() =>
          toast({
            position: POSITION,
            duration: DURATION,
            render: () => (
              <Flex
                color={COLOR}
                p={PADDING}
                display={DISPLAY}
                alignItems={ALIGN_ITEMS}
                bg={RED}
              >
                <WarningIcon mr={MARGIN_RIGHT} />
                "Houve um erro de autenticação. Por favor, tente mais tarde.",
              </Flex>
            ),
          })
        );
      setIsLoading(false);
    } else {
      setIsLoading(false);
    } // eslint-disable-next-line
  }, [token, userHabitsList.length, userGroupsList.goals, isToRender]);

  useEffect(() => {
    if (isToRender) {
      setIsToRender(false);
    }
  }, [isToRender]);
  const { pageWidth } = useWindow();

  const haveLoggedUser = localStorage.getItem(USER_TAG);
  if (!auth && !haveLoggedUser) {
    return <Redirect to="/login" />;
  } else if (haveLoggedUser && !auth) {
    setUser(haveLoggedUser);
  }

  if (pageWidth < SCREEN_BREAKPOINT) {
    return (
      <Container>
        <NavMenu />
        <h1 className="nameDash">
          {" "}
          {!isLoading ? "Dashboard" : "Loading ..."}{" "}
        </h1>
        {!isLoading ? (
          <ContainerInfo>
            <Text as="h3"> Meus Hábitos </Text>
            <ul>
              {userHabitsList.map((item) => (
                <Card
                  key={item.id}
                  setGoals={setUserGoalsList}
                  setActivities={setUserActivitiesList}
                  type={TYPE_HABIT}
                  item={item}
                  userHabitsList={userHabitsList}
                  setUserHabitsList={setUserHabitsList}
                  setIsToRender={setIsToRender}
                />
              ))}
            </ul>
            <Text as="h3"> Minhas Metas </Text>
            <ul>
              {userGoalsList.map((goal) => (
                <Card key={goal.id} type={TYPE_GOAL} item={goal} />
              ))}
            </ul>
            <Text as="h3"> Minhas Atividades </Text>
            <ul>
              {userActivitiesList.map((activity) => (
                <Card key={activity.id} type={TYPE_ACTIVITY} item={activity} />
              ))}
            </ul>
          </ContainerInfo>
        ) : (
          <img className="gif" src={gifLoading} alt="loading" />
        )}
        <Footer childrenSize={500}>
          <AddHabit
            setIsToRender={setIsToRender}
            userHabitsList={userHabitsList}
            setUserHabitsList={setUserHabitsList}
          />
        </Footer>
      </Container>
    );
  } else {
    return (
      <Box
        pos="absolute"
        display="flex"
        h="100vh"
        w="100vw"
        top="0"
        bg="gray.100"
      >
        <Box>
          <NavMenu />
        </Box>

        <Box p="3rem 3rem 0 3rem" w="100%" overflowY="scroll">
          <Box display="flex" justifyContent="space-between">
            <Heading
              as="h2"
              fontSize="1.5rem"
              fontFamily="Poppins, sans-serif"
              color="green.500"
            >
              Dashboard
            </Heading>
            <Button
              onClick={onOpen}
              bg="green.500"
              color="white.500"
              textTransform="uppercase"
            >
              <AddIcon mr="1rem" />
              Criar novo hábito
            </Button>
          </Box>
          <Text
            as="span"
            color="gray.300"
            mt="2rem"
            display="block"
            fontWeight="bold"
          >
            Meus hábitos
          </Text>
          <SimpleGrid
            minChildWidth="250px"
            spacing="40px"
            mt="1rem"
            mb="2rem"
            overflow="visible"
          >
            {userHabitsList.map((item) => (
              <Card
                key={item.id}
                setGoals={setUserGoalsList}
                setActivities={setUserActivitiesList}
                type={TYPE_HABIT}
                item={item}
                userHabitsList={userHabitsList}
                setUserHabitsList={setUserHabitsList}
                setIsToRender={setIsToRender}
              />
            ))}
          </SimpleGrid>
          <Text
            as="span"
            color="gray.300"
            mt="2rem"
            display="block"
            fontWeight="bold"
          >
            Meus objetivos
          </Text>
          <SimpleGrid minChildWidth="250px" spacing="40px" mt="1rem" mb="2rem">
            {userGoalsList.map((goal) => (
              <Card
                key={goal.id}
                type={TYPE_GOAL}
                item={goal}
                ownerList={ownerList}
              />
            ))}
          </SimpleGrid>
          <Text
            as="span"
            color="gray.300"
            mt="1rem"
            display="block"
            fontWeight="bold"
          >
            Minhas atividades
          </Text>
          <SimpleGrid minChildWidth="250px" spacing="40px" mt="1rem">
            {userActivitiesList.map((activity) => (
              <Card
                key={activity.id}
                type={TYPE_ACTIVITY}
                item={activity}
                ownerList={ownerList}
              />
            ))}
          </SimpleGrid>
          <Modal isOpen={isOpen} onClose={onClose}>
            <ModalOverlay />
            <ModalContent bg="green.500" color="white.500">
              <ModalHeader>Criar novo hábito</ModalHeader>
              <ModalCloseButton />
              <ModalBody>
                <AddHabit
                  setIsToRender={setIsToRender}
                  userHabitsList={userHabitsList}
                  setUserHabitsList={setUserHabitsList}
                />
              </ModalBody>
            </ModalContent>
          </Modal>
        </Box>
      </Box>
    );
  }
};
export default Dashboard;
