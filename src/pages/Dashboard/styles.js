import styled from "styled-components";

export const Container = styled.section`
  background-color: var(--chakra-colors-gray-100);
  overflow-x: hidden;

  .nameDash {
    padding: 0.6rem 0;
    font-size: 1.4rem;
    color: var(--chakra-colors-green-500);
    text-align: center;
  }

  .gif {
    margin: 0 auto;
    max-width: 100%;
    max-height: 100%;
    width: 5rem;
    height: 5rem;
  }
`;

export const ContainerInfo = styled.section`
  display: flex;
  flex-direction: column;
  background-color: var(--chakra-colors-green-100);
  padding: 1.25rem;
  height: auto;

  ul {
    height: 15rem;
    overflow-y: scroll;
    margin: 1.4rem 0;
    padding: 0 0.8rem;
    li + li {
      margin-top: 1.25rem;
    }
  }
  .last {
    margin-bottom: 4rem;
  }
  h3 {
    font-weight: 700;
    text-align: left;
    position: relative;
    top: 1.6rem;
    left: 1rem;
    margin-bottom: 1rem;
    color: var(--chakra-colors-gray-500);
  }

  h2 {
    color: var(--chakra-colors-green-500);
    text-align: center;
  }
  input {
    background-color: var(--chakra-colors-white-500);
    outline: 0;
    margin: 0.4rem 0;
    width: 100%;
    height: 2.7rem;
  }

  .info::-webkit-scrollbar {
    width: 0.625rem;
  }
  > ::-webkit-scrollbar-track,
  > ::-webkit-scrollbar-thumb {
    border-radius: 0.5rem;
  }
  .gif {
    margin: 0 auto;
    max-width: 100%;
    max-height: 100%;
    width: 5rem;
    height: 5rem;
  }
`;
