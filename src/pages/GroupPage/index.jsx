import NavMenu from "../../components/NavMenu";
import { StyledContainer, ContainerSelected, StyledButton } from "./styles";
import {
  Box,
  Flex,
  Text,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
  Button,
} from "@chakra-ui/react";
import Card from "../../components/Card";
import { USER_TAG } from "../../utils";
import { useUser } from "../../providers/user";
import { Redirect, useParams, useLocation } from "react-router-dom";
import Menu from "../../components/Breadcrumb";
import { useEffect, useState } from "react";
import api from "../../services/api";
import { categoriesColors } from "../../utils/";
import Footer from "../../components/FooterMenu";
import AddGoal from "../../components/AddGoal";
import AddActivity from "../../components/AddActivity";
import { useUserGroups } from "../../providers/groups";
import { SCREEN_BREAKPOINT } from "../../utils";
import { useWindow } from "../../providers/window";
import { AddIcon } from "@chakra-ui/icons";

const GroupComponent = () => {
  const [group, setGroup] = useState({});
  const {
    user: { auth, id },
    setUser,
  } = useUser();

  const { id: groupId } = useParams();

  const { isOpen, onOpen, onClose } = useDisclosure();

  const location = useLocation();
  const [selectedForm, setSelectedForm] = useState(1);
  const handleSelectedForm = (option) => {
    setSelectedForm(option);
  };
  const { userGroupsList } = useUserGroups();
  const [ownerList, setOwnerList] = useState([]);

  const { pageWidth } = useWindow();

  const [creation, setCreation] = useState("");

  useEffect(() => {
    api.get(`/groups/${groupId}/`).then((resp) => setGroup(resp.data));
    //eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (userGroupsList.length > 0) {
      let aux = userGroupsList.map((group) => {
        return {
          ownerId: group.creator.id,
          groupId: group.id,
          category: group.category,
        };
      });
      setOwnerList(aux);
    }
  }, [userGroupsList]);

  const handleGoal = () => {
    setCreation("Criar objetivo");
    onOpen();
  };

  const handleActivity = () => {
    setCreation("Criar atividade");
    onOpen();
  };

  const haveLoggedUser = localStorage.getItem(USER_TAG);
  if (!auth && !haveLoggedUser) {
    return <Redirect to="/login" />;
  } else if (haveLoggedUser && !auth) {
    setUser(haveLoggedUser);
  }

  const { creator, category, activities, description, goals, name } = group;
  const parent = location.pathname.split("/")[1];
  const pathMenu = [
    {
      name: parent === "searchGroups" ? "Procurar Grupos" : "Meus Grupos",
      path: `/${parent}`,
    },
    { name, path: `/searchGroups/groups/${groupId}` },
  ];

  if (pageWidth < SCREEN_BREAKPOINT) {
    return (
      <Box pos="absolute" h="100vh" top="0">
        <Box mt="80px">
          <NavMenu />
        </Box>
        <StyledContainer categoryColor={categoriesColors[category]}>
          <Flex flexDirection="column" mt="5rem">
            <Menu menuOptions={pathMenu} />
            <h1>{name && name[0].toUpperCase() + name.slice(1)}</h1>
            <p>{description ? description : ""}</p>
            <Flex width="fit-content">
              <span>
                Categoria:
                <span className="category">
                  {!!category && category.split("@Hero:")[1]}
                </span>
              </span>
              <label htmlFor="creator">
                Criador:
                <span id="creator" className="creator">
                  {creator
                    ? creator.username.length > 5
                      ? `${creator.username.slice(0, 5)}...`
                      : creator?.username
                    : ""}
                </span>
                <div>{!!creator && creator.username}</div>
              </label>
            </Flex>
            {!!activities &&
              activities.map((item, index) => (
                <Card
                  group={group}
                  setGroup={setGroup}
                  ownerList={ownerList}
                  key={index}
                  type={2}
                  item={item}
                  className="card"
                />
              ))}

            {!!goals &&
              goals.map((item, index) => (
                <Card
                  group={group}
                  setGroup={setGroup}
                  ownerList={ownerList}
                  key={index}
                  type={1}
                  item={item}
                  className="card"
                />
              ))}
          </Flex>
        </StyledContainer>
        {group.creator?.id === id && (
          <Footer childrenSize={400}>
            <ContainerSelected>
              <StyledButton
                onClick={() => handleSelectedForm(1)}
                variant="outline"
                color="white.500"
                background="transparent"
              >
                {" "}
                Meta{" "}
              </StyledButton>

              <StyledButton
                onClick={() => handleSelectedForm(2)}
                variant="outline"
                color="white.500"
                background="transparent"
              >
                {" "}
                Atividade{" "}
              </StyledButton>
            </ContainerSelected>
            {selectedForm === 1 ? (
              <AddGoal group={group} setGroup={setGroup} />
            ) : (
              <AddActivity group={group} setGroup={setGroup} />
            )}
          </Footer>
        )}
      </Box>
    );
  } else {
    return (
      <Box
        pos="absolute"
        display="flex"
        h="100vh"
        w="100vw"
        top="0"
        bg="gray.100"
      >
        <NavMenu />
        <Box p="3rem 3rem 0 3rem" w="100%" overflowY="scroll">
          <StyledContainer categoryColor={categoriesColors[category]}>
            <Flex flexDirection="column">
              <Box display="flex" justifyContent="space-between">
                <Menu menuOptions={pathMenu} />

                <Box gridColumnGap="1rem" display="flex" position='absolute' top='30px' right='30px'>
                  <Button
                    onClick={handleGoal}
                    bg="green.500"
                    color="white.500"
                    textTransform="uppercase"
                  >
                    <AddIcon mr="1rem" />
                    Objetivo
                  </Button>
                  <Button
                    onClick={handleActivity}
                    bg="green.500"
                    color="white.500"
                    textTransform="uppercase"
                  >
                    <AddIcon mr="1rem" />
                    Atividade
                  </Button>
                </Box>
              </Box>
              <Box>
                <Text
                  as="h2"
                  mt="1.5rem"
                  fontSize="1.5rem"
                  fontWeight="700"
                  color="#414141"
                >
                  {name && name[0].toUpperCase() + name.slice(1)}
                </Text>
                <Modal isOpen={isOpen} onClose={onClose}>
                  <ModalOverlay />
                  <ModalContent bg="green.500" color="white.500">
                    <ModalHeader>{creation}</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>
                      {creation === "Criar objetivo" ? (
                        <AddGoal group={group} setGroup={setGroup} />
                      ) : (
                        <AddActivity group={group} setGroup={setGroup} />
                      )}
                    </ModalBody>
                  </ModalContent>
                </Modal>
              </Box>
              <Text>{description ? description : ""}</Text>
              <Flex>
                <Box>
                  Categoria:
                  <span className="category">
                    {!!category && category.split("@Hero:")[1]}
                  </span>
                </Box>
                <label htmlFor="creator">
                  Criador:{" "}
                  <span id="creator" className="creator">
                    {creator
                      ? creator.username.length > 5
                        ? `${creator.username.slice(0, 5)}...`
                        : creator?.username
                      : ""}
                  </span>
                  <div>{!!creator && creator.username}</div>
                </label>
              </Flex>
              {!!activities &&
                activities.map((item, index) => (
                  <Card
                    group={group}
                    setGroup={setGroup}
                    ownerList={ownerList}
                    key={index}
                    type={2}
                    item={item}
                  />
                ))}

              {!!goals &&
                goals.map((item, index) => (
                  <Card
                    group={group}
                    setGroup={setGroup}
                    ownerList={ownerList}
                    key={index}
                    type={1}
                    item={item}
                  />
                ))}
            </Flex>
          </StyledContainer>
        </Box>
      </Box>
    );
  }
};

export default GroupComponent;
