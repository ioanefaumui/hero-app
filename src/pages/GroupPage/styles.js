import styled from "styled-components";
import { Button } from "@chakra-ui/react";

export const StyledContainer = styled.main`
  margin-top: -80px;
  min-width: 100vw;
  height: 100%;
  background: var(--chakra-colors-gray-100);
  font-family: "Poppins", sans-serif;

  h1,
  & > h2,
  h3,
  h4,
  h5 {
    color: #414141;
    font-family: "Nokora", sans-serif;
  }

  h1 {
    font-size: 1.625rem;
    font-weight: bold;
  }

  & > div > p {
    color: #777;
    margin: 1rem 0;
    font-size: 1.2rem;
    line-height: 22px;
    font-weight: 700;
    font-family: "Nunito", sans-serif;
  }

  label {
    margin-left: 0.8rem;
    margin-bottom: 1rem;
  }

  nav > ol li a,
  nav > ol li a h2 {
    max-width: 7.5rem;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    font-size: 1rem;
  }

  .category {
    background-color: ${({ categoryColor }) => categoryColor};
    color: #fff;
    font-size: 11px;
    text-transform: uppercase;
    padding: 0.2rem 0.3rem;
    margin-left: 0.5rem;
    border-radius: 2px;
  }

  .creator {
    color: #319795;
    font-weight: bold;
    word-break: break-all;
  }

  label .creator + div {
    background: #319795;
    color: #fff;
    font-size: 0.9rem;
    padding: 0.2rem 0.5rem;
    transform: scaleY(0);
    transform-origin: top;
    transition: transform 0.6s;
    position: absolute;
    z-index: 3;
  }
  /* label:hover .creator + div {
    transform: scaleY(1);
    transform-origin: top;
    transition: transform 0.6s;
  } */

  @media only screen and (max-width: 900px) {
    padding: 1.3rem;
    padding-top: 2rem;
    & > div > div > span {
      color: #414141;
      font-size: 1rem;
      font-weight: 600;
    }
    .card {
      width: 85%;
    }
  }
  @media only screen and (max-width: 350px) {
    .card {
      width: 100%;
    }
  }

  @media only screen and (min-width: 350px) {
    nav > ol li a {
      max-width: 15rem;
    }
  }
`;
export const StyledButton = styled(Button)`
  margin: 0 auto;
  &:hover {
    color: #319795;
  }
`;
export const ContainerSelected = styled.section`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  max-width: fit-content;
  margin: 0 auto;
  gap: 2rem;
  height: 40px;
  position: relative;
  top: 30px;

  button {
  }
`;
