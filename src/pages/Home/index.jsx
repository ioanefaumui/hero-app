import { Image } from "@chakra-ui/image";
import { Box, Center, Heading, Text, VStack } from "@chakra-ui/layout";
import { Link, useHistory } from "react-router-dom";
import logo from "../../assets/img/logo.svg";
import arrowDown from "../../assets/img/arrow_down.svg";
import { Button } from "@chakra-ui/button";
import { Container, Fade } from "@chakra-ui/react";
import { useUser } from "../../providers/user";
import { Redirect } from "react-router-dom";
import whitelogo from "../../assets/img/logo-white.svg";
import separatorIcon from "../../assets/img/hero-section-icon.svg";
import { Background, Nav } from "./styles";
import { SCREEN_BREAKPOINT } from "../../utils";
import { useWindow } from "../../providers/window";

export default function App() {
  const history = useHistory();
  const { pageWidth } = useWindow();
  const {
    user: { auth },
  } = useUser();

  if (auth) {
    return <Redirect to="/dashboard" />;
  }

  if (pageWidth < SCREEN_BREAKPOINT) {
    return (
      <Container
        as="main"
        minHeight="100vh"
        width="100vw"
        maxWidth="100vw"
        marginBottom="-60px"
        marginTop="-80px"
      >
        <VStack>
          <Box as="figure" mt="15vh">
            <Image src={logo} alt="Hero" />
            <figcaption className="displayNone">Hero</figcaption>
          </Box>
          <Text
            as="p"
            textAlign="center"
            color="gray.500"
            fontSize="1.5rem"
            w="74%"
            margin="0 auto"
            fontWeight="400"
          >
            <Text as="span" color="green.500" fontWeight="700">
              Salve&nbsp;
            </Text>
            e gerencie seus
            <br />
            hábitos de maneira
            <br />
            <Text as="span" color="green.500" fontWeight="700">
              &nbsp;super&nbsp;
            </Text>
            eficiente.
          </Text>
          <Box as="figure">
            <Image
              src={arrowDown}
              alt="Seta para baixo"
              h="5rem"
              mt="1rem"
              mb="1rem"
            />
            <figcaption className="displayNone">Seta para baixo</figcaption>
          </Box>

          <VStack as="nav">
            <Button
              colorScheme="green"
              variant="solid"
              textTransform="uppercase"
              fontWeight="bold"
              onClick={() => history.push("/register")}
            >
              começar
            </Button>
            <Button
              colorScheme="green"
              variant="outline"
              textTransform="uppercase"
              fontWeight="bold"
              onClick={() => history.push("/login")}
            >
              entrar
            </Button>
          </VStack>
        </VStack>
      </Container>
    );
  } else {
    return (
      <Background>
        <Container
          maxW="78.75rem"
          h="100%"
          color="white.500"
          display="flex"
          flexDir="column"
        >
          <Fade in={true} style={{ transition: "ease 500ms" }}>
            <header>
              <Nav>
                <figure>
                  <img src={whitelogo} alt="Hero" />
                  <figcaption style={{ visibility: "hidden" }}>
                    Hero white logotype
                  </figcaption>
                </figure>
                <Box display="flex" gridColumnGap="2.5rem">
                  <Link to="/login">
                    <Button variant="none" _hover="" fontWeight="black">
                      LOGIN
                    </Button>
                  </Link>
                  <Link to="/register">
                    <Button variant="outline" _hover="" fontWeight="black">
                      REGISTRE-SE
                    </Button>
                  </Link>
                </Box>
              </Nav>
            </header>

            <section style={{ height: "100%" }}>
              <Center h="100%" flexDirection="column" pt="4vh">
                <Heading
                  as="h1"
                  fontFamily="Nunito, sans-serif"
                  size="4xl"
                  textAlign="center"
                  maxW="47.375rem"
                  fontWeight="500"
                  textShadow="0px 5px 12px rgba(0, 0, 0, 1)"
                >
                  <b>Salve</b> e gerencie seus hábitos de maneira <b>super</b>{" "}
                  eficiente
                </Heading>
                <Image src={separatorIcon} mt="2rem" />
                <Link to="register">
                  <Button
                    mt="2rem"
                    size="lg"
                    color="green.500"
                    fontWeight="black"
                    letterSpacing="0.063rem"
                    boxShadow="0px 5px 12px rgba(0, 0, 0, 1)"
                  >
                    COMECE AGORA
                  </Button>
                </Link>
              </Center>
            </section>
          </Fade>
        </Container>
      </Background>
    );
  }
}
