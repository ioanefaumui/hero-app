import styled from "styled-components";
import background from "../../assets/img/landingpage-bg.jpg";

export const Background = styled.div`
  position: absolute;
  display: flex;
  height: 100vh;
  min-width: 100vw;
  top: 0;
  background: url(${background});
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
`;

export const Nav = styled.nav`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 32px 0;

  figure {
    display: flex;
    position: relative;
  }

  figcaption {
    position: absolute;
    height: 100%;
  }


`;
