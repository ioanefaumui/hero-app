import { Link, Redirect, useHistory } from "react-router-dom";
import {
	Flex as HStack,
	Text,
	Input,
	InputGroup,
	InputRightElement,
	IconButton,
	Box,
	useToast,
	Button,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import {
	ArrowRightIcon,
	ViewIcon,
	ViewOffIcon,
	WarningIcon,
} from "@chakra-ui/icons";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import api from "../../services/api";
import { loginSchema } from "../../schemas/schemas";
import { SCREEN_BREAKPOINT, toastConfig, USER_TAG } from "../../utils";
import { useUser } from "../../providers/user";
import { Container, Content, AnimationContainer } from "./styles";
import { useWindow } from "../../providers/window";
import logo from "../../assets/img/logo.svg";

const Login = () => {
	const {
		user,
		user: { auth },
		setUser,
	} = useUser();

	const { pageWidth } = useWindow();
	const [show, setShow] = useState(false);
	const handleClick = () => setShow(!show);
	const toast = useToast();
	const {
		register,
		handleSubmit,
		formState: { errors },
		reset,
	} = useForm({ resolver: yupResolver(loginSchema) });
	const history = useHistory();

	const handleForm = data => {
		const {
			DURATION,
			POSITION,
			COLOR,
			PADDING,
			DISPLAY,
			ALIGN_ITEMS,
			MARGIN_RIGHT,
			RED,
		} = toastConfig;
		api
			.post("/sessions/", data)
			.then(response => {
				const { access } = response.data;
				localStorage.clear();
				setUser({
					...user,
					token: access,
				});
				reset();
				history.push("/dashboard");
			})
			.catch(err =>
				toast({
					position: POSITION,
					duration: DURATION,
					render: () => (
						<Box
							color={COLOR}
							p={PADDING}
							display={DISPLAY}
							alignItems={ALIGN_ITEMS}
							bg={RED}
						>
							<WarningIcon mr={MARGIN_RIGHT} />
							Senha não confere com o usuário.
						</Box>
					),
				}),
			);
	};

	useEffect(() => {
		const haveLoggedUser = localStorage.getItem(USER_TAG);
		if (!auth && !haveLoggedUser) {
			return <Redirect to="/login" />;
		} else if (haveLoggedUser && !auth) {
			setUser(haveLoggedUser);
		}
		// eslint-disable-next-line
	}, []);
	if (auth) {
    return <Redirect to="/dashboard" />;
  }

	if (pageWidth < SCREEN_BREAKPOINT) {
		//MOBILE VERSION:
		return (
			<Container as="main" minHeight="100vh" width="100vw" maxWidth="100vw">
				<Content>
					<Text
						as="h2"
						color="green.500"
						fontSize="2rem"
						margin="1.25rem 0"
						fontWeight="700"
					>
						Faça seu login
					</Text>

					<form onSubmit={handleSubmit(handleForm)}>
						<HStack
							bg="green.500"
							direction="column"
							borderRadius="0.5rem"
							padding="1.25rem 0.9375rem 1.875rem 0.9375rem"
							marginBottom="1.5625rem"
						>
							<Text
								fontSize="0.9rem"
								margin="0.3125rem 0"
								fontWeight="500"
								color="white.500"
							>
								Seu nome de usuário{" "}
								{errors.username?.message ? (
									<Text as="span" color="red.500">
										- {errors.username?.message}
									</Text>
								) : (
									<Text as="span" color="red.500">
										*
									</Text>
								)}
							</Text>
							<Input
								{...register("username")}
								_focus=""
								variant="filled"
								placeholder="Digite seu usuário"
								margin="0 0 0.625rem 0"
							/>

							<Text
								fontSize="0.9rem"
								margin="0.3125rem 0"
								fontWeight="500"
								color="white.500"
							>
								Sua senha{" "}
								{errors.password?.message ? (
									<Text as="span" color="red.500">
										- {errors.password?.message}
									</Text>
								) : (
									<Text as="span" color="red.500">
										*
									</Text>
								)}
							</Text>
							<InputGroup size="md">
								<Input
									{...register("password")}
									_focus=""
									variant="filled"
									pr="4.5rem"
									type={show ? "text" : "password"}
									placeholder="Digite sua senha"
								/>
								<InputRightElement
									color="gray.500"
									width="1rem"
									onClick={handleClick}
									margin="0 0.625rem"
								>
									{show ? <ViewIcon /> : <ViewOffIcon />}
								</InputRightElement>
							</InputGroup>
						</HStack>
						<HStack direction="row" justify="flex-end">
							<IconButton
								w="3.125rem"
								bg="green.500"
								border="none"
								color="white.500"
								aria-label="Send form"
								type="submit"
								icon={<ArrowRightIcon />}
							/>
						</HStack>
					</form>
					<Text
						as="p"
						color="green.500"
						fontSize="1rem"
						margin="1.875rem 0 1.25rem 0"
						fontWeight="500"
						textAlign="center"
					>
						Não tem uma conta? Faça seu{" "}
						<Text
							as="span"
							color="green.500"
							fontWeight="700"
							fontSize="1.1rem"
							margin="0.125rem"
						>
							<Link to="/register">Cadastro</Link>
						</Text>
						.
					</Text>

					<Text
						as="p"
						color="green.500"
						fontSize="1rem"
						fontWeight="500"
						textAlign="center"
					>
						ir para{" "}
						<Text
							as="span"
							color="green.500"
							fontWeight="700"
							fontSize="1.1rem"
							margin="0.125rem"
						>
							<Link to="/">Home</Link>
						</Text>
						.
					</Text>
				</Content>
			</Container>
		);
	} else {
		//DESKTOP VERSION:
		return (
			<Container as="main" minHeight="100vh" width="100vw" maxWidth="100vw">
				<HStack
					position="fixed"
					width="100%"
					height="80px"
					top="0"
					left="0"
					zIndex="2"
					bg="white.500"
					justify="space-between"
					align="center"
				>
					<Link to="/">
						<figure>
							<img src={logo} alt="hero" />
							<figcaption className="displayNone">Hero</figcaption>
						</figure>
					</Link>
					<Box>
						<Button
							mr="4rem"
							colorScheme="green"
							variant="outline"
							borderColor="white.500"
							textTransform="uppercase"
							fontWeight="bold"
							onClick={() => history.push("/home")}
						>
							home
						</Button>
						<Button
							mr="4rem"
							colorScheme="green"
							variant="outline"
							textTransform="uppercase"
							fontWeight="bold"
							onClick={() => history.push("/register")}
						>
							registre-se
						</Button>
					</Box>
				</HStack>
				<Text
					as="h2"
					color="green.500"
					fontSize="2rem"
					margin="1.25rem 0"
					fontWeight="700"
					textTransform="capitalize"
				>
					login
				</Text>
				<AnimationContainer>
					<form onSubmit={handleSubmit(handleForm)}>
						<HStack
							bg="green.500"
							direction="column"
							borderRadius="0.5rem"
							padding="1.25rem 0.9375rem 1.875rem 0.9375rem"
							marginBottom="1.5625rem"
						>
							<Text
								fontSize="0.9rem"
								margin="0.3125rem 0"
								fontWeight="500"
								color="white.500"
							>
								Usuário{" "}
								{errors.username?.message ? (
									<Text as="span" color="red.500">
										- {errors.username?.message}
									</Text>
								) : (
									<Text as="span" color="red.500">
										*
									</Text>
								)}
							</Text>
							<Input
								{...register("username")}
								_focus=""
								variant="filled"
								placeholder="Digite seu usuário"
								margin="0 0 0.625rem 0"
							/>

							<Text
								fontSize="0.9rem"
								margin="0.3125rem 0"
								fontWeight="500"
								color="white.500"
							>
								Senha{" "}
								{errors.password?.message ? (
									<Text as="span" color="red.500">
										- {errors.password?.message}
									</Text>
								) : (
									<Text as="span" color="red.500">
										*
									</Text>
								)}
							</Text>
							<InputGroup size="md">
								<Input
									{...register("password")}
									_focus=""
									variant="filled"
									pr="4.5rem"
									type={show ? "text" : "password"}
									placeholder="Digite sua senha"
								/>
								<InputRightElement
									color="gray.500"
									width="1rem"
									onClick={handleClick}
									margin="0 0.625rem"
								>
									{show ? <ViewIcon /> : <ViewOffIcon />}
								</InputRightElement>
							</InputGroup>
						</HStack>
						<HStack
							justifyContent="space-between"
							alignItems="center"
							fontSize=".85rem"
						>
							<Text
								as="p"
								color="gray.100"
								margin="1.875rem 0 1.25rem 0"
								fontWeight="500"
								textAlign="center"
							>
								Não possui uma conta?
								<Text
									as="span"
									color="gray.100"
									fontWeight="700"
									margin="0.125rem"
								>
									<Link to="/register"> Cadastrar</Link>
								</Text>
								.
							</Text>
							<Button
								type="submit"
								variant="filled"
								color="green.500"
								bg="white.500"
								textTransform="uppercase"
								fontWeight="bold"
							>
								entrar
							</Button>
						</HStack>
					</form>
				</AnimationContainer>
			</Container>
		);
	}
};

export default Login;
