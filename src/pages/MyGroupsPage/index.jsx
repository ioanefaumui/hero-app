import NavMenu from "../../components/NavMenu";
import FooterComponent from "../../components/FooterMenu";
import AddGroups from "../../components/AddGroups";
import { Container } from "./styles";
import { Redirect } from "react-router-dom";
import { useUser } from "../../providers/user";
import BreadComponent from "../../components/Breadcrumb";
import {
  Box,
  VStack,
  useToast,
  SimpleGrid,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
  Button,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { USER_TAG, toastConfig } from "../../utils";
import { AddIcon, CheckCircleIcon, WarningIcon } from "@chakra-ui/icons";
import api from "../../services/api";
import GroupCardComponent from "../../components/GroupCard";
import { SCREEN_BREAKPOINT } from "../../utils";
import { useWindow } from "../../providers/window";

const MyGroupsPage = () => {
  const {
    user: { auth, token },
    setUser,
  } = useUser();
  const toast = useToast();
  const [groupsList, setGroupsList] = useState([]);
  const [isToFetch, setIsToFetch] = useState(true);

  const { pageWidth } = useWindow();

  const { isOpen, onOpen, onClose } = useDisclosure();

  const loadGroups = () => {
    if (token) {
      api
        .get("/groups/subscriptions/", {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((response) => {
          const { data } = response;

          if (data.length > 0) {
            setGroupsList(data);
          }
          setIsToFetch(false);
        })
        .catch((error) => {
          let errMsg = `Erro ao tentar excluir. Por favor, `;
          if (error.response?.status === 401) {
            errMsg += "refaça seu login.";
          } else {
            errMsg += "tente mais tarde.";
          }
          heroAlert(errMsg, false);
          console.dir(error);
          setIsToFetch(false);
        });
    }
  };

  useEffect(() => {
    if (isToFetch) {
      loadGroups();
    } // eslint-disable-next-line
  }, [groupsList, token]);

  const haveLoggedUser = localStorage.getItem(USER_TAG);
  if (!auth && !haveLoggedUser) {
    return <Redirect to="/login" />;
  } else if (haveLoggedUser && !auth) {
    setUser(haveLoggedUser);
  }

  const breadCrumbOptions = [
    {
      name: "Dashboard",
      path: "/dashboard",
    },
    {
      name: "Meus grupos",
      path: "/mygroups",
    },
  ];

  const heroAlert = (msg, isSuccess = true) => {
    const {
      DURATION,
      POSITION,
      COLOR,
      PADDING,
      DISPLAY,
      ALIGN_ITEMS,
      MARGIN_RIGHT,
      GREEN,
      RED,
    } = toastConfig;

    if (isSuccess) {
      toast({
        position: POSITION,
        duration: DURATION,
        render: () => (
          <Box
            color={COLOR}
            p={PADDING}
            display={DISPLAY}
            alignItems={ALIGN_ITEMS}
            bg={GREEN}
          >
            <CheckCircleIcon mr={MARGIN_RIGHT} />
            {msg}
          </Box>
        ),
      });
    } else {
      toast({
        position: POSITION,
        duration: DURATION,
        render: () => (
          <Box
            color={COLOR}
            p={PADDING}
            display={DISPLAY}
            alignItems={ALIGN_ITEMS}
            bg={RED}
          >
            <WarningIcon mr={MARGIN_RIGHT} />
            {msg}
          </Box>
        ),
      });
    }
  };

  if (pageWidth < SCREEN_BREAKPOINT) {
    return (
      <Container>
        <NavMenu></NavMenu>
        <Box as="main" m="2%">
          <BreadComponent menuOptions={breadCrumbOptions} />
        </Box>
        <VStack as="ul">
          {groupsList &&
            groupsList.map((item) => {
              return (
                <GroupCardComponent
                  key={item.id}
                  group={item}
                  loadGroups={loadGroups}
                />
              );
            })}
        </VStack>
        <FooterComponent childrenSize={400}>
          <AddGroups loadGroups={loadGroups} />
        </FooterComponent>
      </Container>
    );
  } else {
    return (
      <Box
        pos="absolute"
        display="flex"
        h="100vh"
        w="100vw"
        top="0"
        bg="gray.100"
      >
        <Box>
          <NavMenu />
        </Box>
        <Box p="3rem 3rem 0 3rem" w="100%" overflowY="scroll">
          <Box display="flex" justifyContent="space-between">
            <BreadComponent menuOptions={breadCrumbOptions} />
            <Button
              onClick={onOpen}
              bg="green.500"
              color="white.500"
              textTransform="uppercase"
            >
              <AddIcon mr="1rem" />
              Criar novo grupo
            </Button>
          </Box>

          <SimpleGrid minChildWidth="250px" spacing="40px" mt="2rem" mb="2rem">
            {groupsList &&
              groupsList.map((item) => {
                return (
                  <GroupCardComponent
                    key={item.id}
                    group={item}
                    loadGroups={loadGroups}
                  />
                );
              })}
          </SimpleGrid>

          <Modal isOpen={isOpen} onClose={onClose}>
            <ModalOverlay />
            <ModalContent bg="green.500" color="white.500">
              <ModalHeader>Criar novo grupo</ModalHeader>
              <ModalCloseButton />
              <ModalBody>
                <AddGroups loadGroups={loadGroups} />
              </ModalBody>
            </ModalContent>
          </Modal>
        </Box>
      </Box>
    );
  }
};

export default MyGroupsPage;
