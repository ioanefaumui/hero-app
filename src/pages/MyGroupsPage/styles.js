import styled from "styled-components";
import { Select } from "@chakra-ui/react";

export const Container = styled.div`
  background-color: var(--chakra-colors-gray-100);
  min-height: 100vh;
`;

export const SelectStyled = styled(Select)`
  background-color: ${({ bgColor }) => {
    return bgColor;
  }};
  width: fit-content;
  height: fit-content;
  text-transform: uppercase;
`;
