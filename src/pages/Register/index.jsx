import { Link, Redirect, useHistory } from "react-router-dom";
import {
  Flex as HStack,
  Text,
  Input,
  InputGroup,
  InputRightElement,
  IconButton,
  useToast,
  Box,
  Button,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import {
  ArrowRightIcon,
  CheckIcon,
  ViewIcon,
  ViewOffIcon,
  WarningIcon,
} from "@chakra-ui/icons";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import api from "../../services/api";
import { useUser } from "../../providers/user";
import { registerSchema } from "../../schemas/schemas";
import { toastConfig, USER_TAG, SCREEN_BREAKPOINT } from "../../utils";
import { Container, Content, AnimationContainer } from "./styles";
import { useWindow } from "../../providers/window";
import logo from "../../assets/img/logo.svg";

const Register = () => {
  const {
    user: { auth },
    setUser,
  } = useUser();
  const { pageWidth } = useWindow();

  const [show, setShow] = useState(false);
  const handleClick = () => setShow(!show);

  const toast = useToast();

  const history = useHistory();

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({ resolver: yupResolver(registerSchema) });

  const handleForm = (data) => {
    const {
      DURATION,
      POSITION,
      COLOR,
      PADDING,
      DISPLAY,
      ALIGN_ITEMS,
      MARGIN_RIGHT,
      GREEN,
      RED,
    } = toastConfig;

    api
      .post("/users/", data)
      .then((_) => {
        toast({
          position: POSITION,
          duration: DURATION,
          render: () => (
            <Box
              color={COLOR}
              p={PADDING}
              display={DISPLAY}
              alignItems={ALIGN_ITEMS}
              bg={GREEN}
            >
              <CheckIcon mr={MARGIN_RIGHT} />
              Conta criada com acesso.
            </Box>
          ),
        });
        reset();
        history.push("/login");
      })
      .catch((err) =>
        toast({
          position: POSITION,
          duration: DURATION,
          render: () => (
            <Box
              color={COLOR}
              p={PADDING}
              display={DISPLAY}
              alignItems={ALIGN_ITEMS}
              bg={RED}
            >
              <WarningIcon mr={MARGIN_RIGHT} />
              Erro ao criar a conta. Por favor, tente outro e-mail.
            </Box>
          ),
        })
      );
  };

  useEffect(() => {
    const haveLoggedUser = localStorage.getItem(USER_TAG);
    if (!auth && !haveLoggedUser) {
      return <Redirect to="/login" />;
    } else if (haveLoggedUser && !auth) {
      setUser(haveLoggedUser);
    }
    // eslint-disable-next-line
  }, []);
  
  if (auth) {
    return <Redirect to="/dashboard" />;
  }

  if (pageWidth < SCREEN_BREAKPOINT) {
    return (
      //MOBILE VERSION:
      <Container>
        <Content direction="column" minW="18.75rem">
          <Text
            as="h2"
            color="white.500"
            fontSize="2rem"
            margin="1.25rem 0"
            fontWeight="700"
          >
            Cadastre-se
          </Text>

          <form onSubmit={handleSubmit(handleForm)}>
            <HStack
              bg="white.500"
              direction="column"
              borderRadius="0.5rem"
              padding="1.25rem 0.9375rem 1.875rem 0.9375rem"
              marginBottom="1.5625rem"
            >
              <Text
                fontSize="0.9rem"
                margin="0.3125rem 0"
                fontWeight="500"
                color="gray.500"
              >
                Seu nome de usuário{" "}
                {errors.username?.message ? (
                  <Text as="span" color="red.500">
                    - {errors.username?.message}
                  </Text>
                ) : (
                  <Text as="span" color="red.500">
                    *
                  </Text>
                )}
              </Text>
              <Input
                {...register("username")}
                _focus=""
                variant="filled"
                placeholder="Escolha seu usuário"
                margin="0 0 0.625rem 0"
              />

              <Text
                fontSize="0.9rem"
                margin="0.3125rem 0"
                fontWeight="500"
                color="gray.500"
              >
                Seu e-mail{" "}
                {errors.email?.message ? (
                  <Text as="span" color="red.500">
                    - {errors.email?.message}
                  </Text>
                ) : (
                  <Text as="span" color="red.500">
                    *
                  </Text>
                )}
              </Text>
              <Input
                {...register("email")}
                _focus=""
                variant="filled"
                placeholder="Digite seu melhor e-mail"
                margin="0 0 0.625rem 0"
              />

              <Text
                fontSize="0.9rem"
                margin="0.3125rem 0"
                fontWeight="500"
                color="gray.500"
              >
                Sua senha{" "}
                {errors.password?.message ? (
                  <Text as="span" color="red.500">
                    - {errors.password?.message}
                  </Text>
                ) : (
                  <Text as="span" color="red.500">
                    *
                  </Text>
                )}
              </Text>
              <InputGroup size="md">
                <Input
                  {...register("password")}
                  _focus=""
                  variant="filled"
                  pr="4.5rem"
                  type={show ? "text" : "password"}
                  placeholder="Uma senha bem segura"
                />
                <InputRightElement
                  color="gray.500"
                  width="1rem"
                  onClick={handleClick}
                  margin="0 0.625rem"
                >
                  {show ? <ViewIcon /> : <ViewOffIcon />}
                </InputRightElement>
              </InputGroup>
            </HStack>
            <HStack direction="row" justify="flex-end">
              <IconButton
                w="3.125rem"
                bg="white.500"
                border="none"
                color="green.500"
                aria-label="Send form"
                type="submit"
                icon={<ArrowRightIcon />}
              />
            </HStack>
          </form>
          <Text
            as="p"
            color="white.500"
            fontSize="1rem"
            margin="1.875rem 0 1.25rem 0"
            fontWeight="500"
            textAlign="center"
          >
            Já tem uma conta? Faça seu{" "}
            <Text
              as="span"
              color="gray.100"
              fontWeight="700"
              fontSize="1.1rem"
              margin="0.125rem"
            >
              <Link to="/login">Login</Link>
            </Text>
            .
          </Text>

          <Text
            as="p"
            color="white.500"
            fontSize="1rem"
            fontWeight="500"
            textAlign="center"
          >
            Ir para{" "}
            <Text
              as="span"
              color="gray.100"
              fontWeight="700"
              fontSize="1.1rem"
              margin="0.125rem"
            >
              <Link to="/">Home</Link>
            </Text>
            .
          </Text>
        </Content>
      </Container>
    );
  } else {
    //DESKTOP VERSION:
    return (
      <Container as="main" minHeight="100vh" width="100vw" maxWidth="100vw">
        <HStack
          position="fixed"
          width="100%"
          height="80px"
          top="0"
          left="0"
          zIndex="2"
          bg="white.500"
          justify="space-between"
          align="center"
        >
          <Link to="/">
            <figure>
              <img src={logo} alt="hero" />
              <figcaption className="displayNone">Hero</figcaption>
            </figure>
          </Link>
          <Box>
            <Button
              mr="4rem"
              colorScheme="green"
              variant="outline"
              borderColor="white.500"
              textTransform="uppercase"
              fontWeight="bold"
              onClick={() => history.push("/home")}
            >
              home
            </Button>
            <Button
              mr="4rem"
              colorScheme="green"
              variant="outline"
              textTransform="uppercase"
              fontWeight="bold"
              onClick={() => history.push("/login")}
            >
              login
            </Button>
          </Box>
        </HStack>
        <Text
          as="h2"
          color="green.500"
          fontSize="2rem"
          margin="1.25rem 0"
          fontWeight="700"
          textTransform="capitalize"
        >
          cadastro
        </Text>
        <AnimationContainer>
          <form onSubmit={handleSubmit(handleForm)}>
            <HStack
              bg="green.500"
              direction="column"
              borderRadius="0.5rem"
              padding="1.25rem 0.9375rem 1.875rem 0.9375rem"
              marginBottom="1.5625rem"
            >
              <Text
                fontSize="0.9rem"
                margin="0.3125rem 0"
                fontWeight="500"
                color="white.500"
              >
                Seu nome de usuário{" "}
                {errors.username?.message ? (
                  <Text as="span" color="red.500">
                    - {errors.username?.message}
                  </Text>
                ) : (
                  <Text as="span" color="red.500">
                    *
                  </Text>
                )}
              </Text>
              <Input
                {...register("username")}
                _focus=""
                variant="filled"
                placeholder="Escolha seu usuário"
                margin="0 0 0.625rem 0"
              />

              <Text
                fontSize="0.9rem"
                margin="0.3125rem 0"
                fontWeight="500"
                color="white.500"
              >
                Seu e-mail{" "}
                {errors.email?.message ? (
                  <Text as="span" color="red.500">
                    - {errors.email?.message}
                  </Text>
                ) : (
                  <Text as="span" color="red.500">
                    *
                  </Text>
                )}
              </Text>
              <Input
                {...register("email")}
                _focus=""
                variant="filled"
                placeholder="Digite seu melhor e-mail"
                margin="0 0 0.625rem 0"
              />

			  <div className="errorText">	
				<Text
					fontSize="0.9rem"
					margin="0.3125rem 0"
					fontWeight="500"
					color="white.500"
				>
					Sua senha{" "}
					{errors.password?.message ? (
					<Text as="span" color="red.500">
						- {errors.password?.message}
					</Text>
					) : (
					<Text as="span" color="red.500">
						*
					</Text>
					)}
				</Text>
			  </div>
              <InputGroup size="md">
                <Input
                  {...register("password")}
                  _focus=""
                  variant="filled"
                  pr="4.5rem"
                  type={show ? "text" : "password"}
                  placeholder="Uma senha bem segura"
                />
                <InputRightElement
                  color="gray.500"
                  width="1rem"
                  onClick={handleClick}
                  margin="0 0.625rem"
                >
                  {show ? <ViewIcon /> : <ViewOffIcon />}
                </InputRightElement>
              </InputGroup>
            </HStack>
            <HStack
              justifyContent="space-between"
              alignItems="center"
              fontSize=".85rem"
            >
              <Text
                as="p"
                color="gray.100"
                margin="1.875rem 0 1.25rem 0"
                fontWeight="500"
                textAlign="center"
              >
                Já possui uma conta?
                <Text
                  as="span"
                  color="gray.100"
                  fontWeight="700"
                  margin="0.125rem"
                >
                  <Link to="/login"> Login</Link>
                </Text>
                .
              </Text>
              <Button
                type="submit"
                variant="filled"
                color="green.500"
                bg="white.500"
                textTransform="uppercase"
                fontWeight="bold"
              >
                cadastrar
              </Button>
            </HStack>
          </form>
        </AnimationContainer>
      </Container>
    );
  }
};

export default Register;
