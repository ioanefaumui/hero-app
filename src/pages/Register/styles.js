import styled, { keyframes } from "styled-components";

export const Container = styled.div`
    background-color: var(--chakra-colors-green-500);
    min-height: 100vh;
    max-width: 100vw;
	display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    margin-top: -80px;
    margin-bottom: -60px;

    @media (min-width: 900px) {
        background-color: var(--chakra-colors-white-500);
		div {
			a {
				figure {
					img {
						margin-left: 4rem;
						width: 6rem;
					}
				}
			}
		}
	}
`;

export const Content = styled.div`
	display: flex;
    flex-direction: column;
    min-width: 18.75rem;
`;

const appearFromLeft = keyframes`
    from {
        opacity: 0;
        transform: translateX(-100px)
    }
    to {
        opacity: 1;
        transform: translateX(0px)
    }
`;

export const AnimationContainer = styled.div`
    animation: ${appearFromLeft} 1s;
    background-color: var(--chakra-colors-green-500);
    border-radius: 0.3rem;
    min-width: 28rem;
    padding: 1rem;

    .errorText {
        width: 28rem;
        white-space: wrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }

    input {
        &::placeholder {
            color: var(--chakra-colors-gray-300);
        }
    }
`;