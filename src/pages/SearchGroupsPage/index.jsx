import { useUser } from "../../providers/user";
import { Redirect } from "react-router-dom";
import NavMenu from "../../components/NavMenu";
import {
  Box,
  HStack,
  SimpleGrid,
  Text,
  useToast,
  VStack,
} from "@chakra-ui/react";
import {
  toastConfig,
  USER_TAG,
  groupsCategories,
  categoriesColors,
} from "../../utils";
import BreadComponent from "../../components/Breadcrumb";
import { SelectStyled } from "./styles";
import { useEffect, useState } from "react";
import api from "../../services/api";
import { CheckCircleIcon, WarningIcon } from "@chakra-ui/icons";
import GroupCardComponent from "../../components/GroupCard";
import Pager from "../../components/Pager";
import { SCREEN_BREAKPOINT } from "../../utils";
import { useWindow } from "../../providers/window";

const SearchGroupsPage = () => {
  const {
    HERO_ALL,
    HERO_EDUCATION,
    HERO_HEALTH,
    HERO_KITCHEN,
    HERO_LEISURE,
    HERO_MUSIC,
    HERO_OTHER,
    HERO_READING,
    HERO_SPORT,
    HERO_TRIP,
  } = groupsCategories;

  const {
    user: { auth },
    setUser,
  } = useUser();
  const [bgColor, setBgColor] = useState(null);
  const [selectedCategory, setSelectedCategory] = useState(HERO_ALL);
  const toast = useToast();
  const [groupsList, setGroupsList] = useState([]);
  const [isToFetch, setIsToFetch] = useState(true);
  const [pages, setPages] = useState({ prev: null, next: null, current: 1 });
  const { pageWidth } = useWindow();

  const loadGroups = () => {
    api
      .get(
        `/groups/`,
        { params: { page: pages.current, category: selectedCategory } },
        {}
      )
      .then((response) => {
        const { data } = response;

        if (data.count) {
          setGroupsList(data.results);
          const nextPage = parseInt(data.next?.split("page=")[1]) || null;

          setPages({
            next: data.next,
            prev: data.previous,
            current: nextPage !== null ? nextPage - 1 : pages.current,
          });
        } else {
          setPages({
            next: null,
            prev: null,
            current: 1,
          });
        }

        setIsToFetch(false);
      })
      .catch((error) => {
        let errMsg = `Erro ao tentar excluir. Por favor, `;
        if (error.response?.status === 401) {
          errMsg += "refaça seu login.";
        } else {
          errMsg += "tente mais tarde.";
        }
        heroAlert(errMsg, false);
        console.dir(error);
        setIsToFetch(false);
      });
  };

  useEffect(() => {
    if (isToFetch) {
      if (auth) {
        loadGroups();
      }
    } // eslint-disable-next-line
  }, [groupsList, auth, pages.current]);

  useEffect(() => {}, [pages]);

  const haveLoggedUser = localStorage.getItem(USER_TAG);
  if (!auth && !haveLoggedUser) {
    return <Redirect to="/login" />;
  } else if (haveLoggedUser && !auth) {
    setUser(haveLoggedUser);
  }

  const breadCrumbOptions = [
    {
      name: "Dashboard",
      path: "/dashboard",
    },
    {
      name: "Todos os grupos",
      path: "/searchGroups",
    },
  ];

  const heroAlert = (msg, isSuccess = true) => {
    const {
      DURATION,
      POSITION,
      COLOR,
      PADDING,
      DISPLAY,
      ALIGN_ITEMS,
      MARGIN_RIGHT,
      GREEN,
      RED,
    } = toastConfig;

    if (isSuccess) {
      toast({
        position: POSITION,
        duration: DURATION,
        render: () => (
          <Box
            color={COLOR}
            p={PADDING}
            display={DISPLAY}
            alignItems={ALIGN_ITEMS}
            bg={GREEN}
          >
            <CheckCircleIcon mr={MARGIN_RIGHT} />
            {msg}
          </Box>
        ),
      });
    } else {
      toast({
        position: POSITION,
        duration: DURATION,
        render: () => (
          <Box
            color={COLOR}
            p={PADDING}
            display={DISPLAY}
            alignItems={ALIGN_ITEMS}
            bg={RED}
          >
            <WarningIcon mr={MARGIN_RIGHT} />
            {msg}
          </Box>
        ),
      });
    }
  };

  const changeSelect = (e) => {
    setSelectedCategory(e.target.value);
    setBgColor(categoriesColors[e.target.value]);
    setGroupsList([]);
    setPages({ prev: null, next: null, current: 1 });
    setIsToFetch(true);
    e.target.blur();
  };

  const handlePagesClick = (page) => {
    if (page === "next") {
      const current = parseInt(pages.next.split("page=")[1]);

      setPages({ ...pages, current: current });
    } else {
      if (pages.current === 2) {
        setPages({ ...pages, current: 1 });
      } else {
        const prev = parseInt(pages.prev.split("page=")[1]);

        setPages({ ...pages, current: prev });
      }
    }

    setIsToFetch(true);
  };

  if (pageWidth < SCREEN_BREAKPOINT) {
    return (
      <>
        <NavMenu />
        <Box as="main" m="2%">
          <BreadComponent menuOptions={breadCrumbOptions} />
          <HStack position="relative" w="fit-content">
            <Text
              as="label"
              htmlFor="selectCategory"
              fontSize="1.3rem"
              fontWeight="700"
            >
              Categoria:
            </Text>
            <SelectStyled
              bgColor={bgColor}
              onChange={changeSelect}
              variant="filled"
              id="selectCategory"
            >
              <option defaultValue value={HERO_ALL}>
                Todas
              </option>
              <option value={HERO_KITCHEN}>Cozinha</option>
              <option value={HERO_EDUCATION}>Educação</option>
              <option value={HERO_SPORT}>Esporte</option>
              <option value={HERO_READING}>Leitura</option>
              <option value={HERO_LEISURE}>Lazer</option>
              <option value={HERO_MUSIC}>Música</option>
              <option value={HERO_HEALTH}>Saúde</option>
              <option value={HERO_TRIP}>Viagem</option>
              <option value={HERO_OTHER}>Outra</option>
            </SelectStyled>
          </HStack>
          <Pager pages={pages} handlePagesClick={handlePagesClick} />
        </Box>
        <VStack as="ul">
          {groupsList &&
            groupsList.map((item) => {
              return (
                <GroupCardComponent
                  key={item.id}
                  group={item}
                  loadGroups={loadGroups}
                />
              );
            })}
        </VStack>
      </>
    );
  } else {
    return (
      <Box
        pos="absolute"
        display="flex"
        h="100vh"
        w="100vw"
        top="0"
        bg="gray.100"
      >
        <NavMenu />
        <Box p="3rem 3rem 0 3rem" w="100%" overflowY="scroll">
          <BreadComponent menuOptions={breadCrumbOptions} />
          <HStack position="relative" w="fit-content" m="2rem 0 2.5rem 0">
            <Text
              as="label"
              htmlFor="selectCategory"
              fontSize="1.3rem"
              fontWeight="700"
              fontFamily="Poppins, sans-serif"
              color="#414141"
            >
              Categoria:
            </Text>
            <SelectStyled
              bgColor={bgColor}
              onChange={changeSelect}
              variant="filled"
              id="selectCategory"
              fontWeight="700"
              color="white.500"
            >
              <option defaultValue value={HERO_ALL}>
                Todas
              </option>
              <option value={HERO_KITCHEN} hey={HERO_KITCHEN}>
                Cozinha
              </option>
              <option value={HERO_EDUCATION}>Educação</option>
              <option value={HERO_SPORT}>Esporte</option>
              <option value={HERO_READING}>Leitura</option>
              <option value={HERO_LEISURE}>Lazer</option>
              <option value={HERO_MUSIC}>Música</option>
              <option value={HERO_HEALTH}>Saúde</option>
              <option value={HERO_TRIP}>Viagem</option>
              <option value={HERO_OTHER}>Outra</option>
            </SelectStyled>
          </HStack>
          <Pager pages={pages} handlePagesClick={handlePagesClick} />
          <SimpleGrid
            minChildWidth="250px"
            spacing="40px"
            mb="2rem"
            overflow="visible"
          >
            {groupsList &&
              groupsList.map((item) => {
                return (
                  <GroupCardComponent
                    key={item.id}
                    group={item}
                    loadGroups={loadGroups}
                  />
                );
              })}
          </SimpleGrid>
        </Box>
      </Box>
    );
  }
};

export default SearchGroupsPage;
