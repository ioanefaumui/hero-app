import { Select } from "@chakra-ui/react";
import styled from "styled-components";

export const SelectStyled = styled(Select)`
  cursor: pointer;

  border-radius: 4px;
  padding: 0 2.1rem 0 0.5rem;
  background-color: ${({ bgColor }) =>
    bgColor || "var(--chakra-colors-gray-300)"};
  width: min-content;
  height: fit-content;
  text-transform: uppercase;

  :focus {
    background: ${({ bgColor }) => bgColor || "var(--chakra-colors-gray-300)"};
  }

  option {
    background-color: var(--chakra-colors-white-500);
    color: #414141;
    font-weight: 700;
    border-radius: 4px;
  }
`;
