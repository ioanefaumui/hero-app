import { useEffect } from "react";
import { createContext, useState, useContext } from "react";
import { useToken } from "../Token";
import { useJwt } from "react-jwt";

const UserNumberContext = createContext([]);

export const UserNumberProvider = ({ children }) => {
	const [userNumber, setUserNumber] = useState(0);
	const { token } = useToken();
	const { decodedToken } = useJwt(token);

	useEffect(() => {
		if (token) {
			setUserNumber(decodedToken.user_id);
		}
	}, [token, decodedToken?.user_id]);

	useEffect(() => console.log(userNumber), [userNumber]);
	
	return (
		<UserNumberContext.Provider value={{ userNumber, setUserNumber }}>
			{children}
		</UserNumberContext.Provider>
	);
};

export const useUserNumber = () => useContext(UserNumberContext);
