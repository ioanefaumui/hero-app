import { createContext, useContext, useEffect, useState } from "react";
import api from "../../services/api";
import {
	TAG_USER_ACTIVITIES_LIST,
	TAG_USER_GOAL_LIST,
	TAG_USER_GROUPS_LIST,
} from "../../utils/";
import { useUser } from "../user";

const GroupsCTX = createContext();

export const UserGroupsProvider = ({ children }) => {
	const {
		user: { token, auth },
	} = useUser();
	const [userGroupsList, setUserGroupsList] = useState(
		JSON.parse(localStorage.getItem(TAG_USER_GROUPS_LIST)) || [],
	);
	const [userGoalsList, setUserGoalsList] = useState(
		JSON.parse(localStorage.getItem(TAG_USER_GOAL_LIST)) || [],
	);
	const [userActivitiesList, setUserActivitiesList] = useState(
		JSON.parse(localStorage.getItem(TAG_USER_ACTIVITIES_LIST)) || [],
	);

	useEffect(() => {
		if (auth) {
			api
				.get("/groups/subscriptions/", {
					headers: {
						Authorization: `Bearer ${token}`,
					},
				})
				.then(response => {
					const { data } = response;

					if (data.length > 0) {
						setUserGroupsList(data);
						localStorage.setItem(TAG_USER_GROUPS_LIST, JSON.stringify(data));
					}
				})
				.catch(error => {
					console.error(`Error on groups GET attempt:\n${error}`);
					console.dir(error);
				});
		}
		// eslint-disable-next-line
	}, [auth]);

	useEffect(() => {

		if (auth) {
			const tempGoalList = []; // to avoid multiple same ids into userGoalsList state.
			const tempActivitiesList = []; // to avoid multiple same ids into userActivitiesList state.

			userGroupsList.forEach(({ activities, goals }) => {
				if (activities.length) {
					tempActivitiesList.push(...activities);
				}
				if (goals.length) {
					tempGoalList.push(...goals);
				}
			});
			localStorage.setItem(
				TAG_USER_ACTIVITIES_LIST,
				JSON.stringify(tempActivitiesList),
			);
			setUserActivitiesList(tempActivitiesList);
			setUserGoalsList(tempGoalList);
			localStorage.setItem(TAG_USER_GOAL_LIST, JSON.stringify(tempGoalList));
		}
		// eslint-disable-next-line
	}, [auth, userGroupsList]);

	return (
		<GroupsCTX.Provider
			value={{
				userGroupsList,
				setUserGroupsList,
				setUserActivitiesList,
				setUserGoalsList,
				userGoalsList,
				userActivitiesList,

			}}
		>
			{children}
		</GroupsCTX.Provider>
	);
};

export const useUserGroups = () => useContext(GroupsCTX);
