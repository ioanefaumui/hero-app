import { UserProvider } from "./user";
import { MenusProvider } from "./menus";
import { UserGroupsProvider } from "./groups";
import { WindowProvider } from "./window";

const Providers = ({ children }) => {
	return (
		<WindowProvider>
			<UserProvider>
				<MenusProvider>
					<UserGroupsProvider>
						{children}
					</UserGroupsProvider>
				</MenusProvider>
			</UserProvider>
		</WindowProvider>
	);
};

export default Providers;
