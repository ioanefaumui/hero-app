import { createContext, useContext, useState, useEffect } from "react";

const menusCTX = createContext();

export const MenusProvider = ({ children }) => {
	const [topMenuOpened, setTopMenuOpened] = useState(false);
	const [bottomMenuOpened, setBottomMenuOpened] = useState(false);

	useEffect(() => {
		if (topMenuOpened === true) {
			document.body.style.overflowY = "hidden";
			setBottomMenuOpened(false);
		}
		if (bottomMenuOpened === true) {
			document.body.style.overflowY = "hidden";
			setTopMenuOpened(false);
		}
		if (topMenuOpened === false && bottomMenuOpened === false) {
			document.body.style.overflowY = "auto";
		}
	}, [topMenuOpened, bottomMenuOpened]);

	const toggleBottomMenu = () => {
		if (topMenuOpened === true) {
			setTopMenuOpened(false);
		}
		setBottomMenuOpened(!bottomMenuOpened);
	};

	const toggleTopMenu = () => {
		if (bottomMenuOpened === true) {
			setBottomMenuOpened(false);
		}
		setTopMenuOpened(!topMenuOpened);
	};
	return (
		<menusCTX.Provider
			value={{
				toggleTopMenu,
				toggleBottomMenu,
				topMenuOpened,
				bottomMenuOpened,
				setTopMenuOpened,
				setBottomMenuOpened,
			}}
		>
			{children}
		</menusCTX.Provider>
	);
};

export const useMenus = () => useContext(menusCTX);
