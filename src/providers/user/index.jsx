import { createContext, useContext, useState, useEffect } from "react";
import { useJwt } from "react-jwt";
import { USER_TAG } from "../../utils/";

const userCTX = createContext();

export const UserProvider = ({ children }) => {
	const haveLoggedUser = JSON.parse(localStorage.getItem(USER_TAG));
  const [user, setUser] = useState(
		haveLoggedUser || {
			token: "",
			id: undefined,
			auth: false,
		},
	);

	const { decodedToken } = useJwt(user.token);

	useEffect(() => {
		// const haveLoggedUser = JSON.parse(localStorage.getItem(USER_TAG));
		if (!user.auth && haveLoggedUser) {
			setUser(haveLoggedUser);
		} else if (user.token === "") {
			setUser({
				token: "",
				id: undefined,
				auth: false,
			});
		}

		if (user.token && decodedToken !== null) {
			const attUser = {
				token: user.token,
				id: decodedToken.user_id,
				auth: true,
			};
			setUser(attUser);
			localStorage.setItem(USER_TAG, JSON.stringify(attUser));
		}
		// eslint-disable-next-line
	}, [user.token, decodedToken]);

	return (
		<userCTX.Provider value={{ user, setUser }}>{children}</userCTX.Provider>
	);
};

export const useUser = () => useContext(userCTX);
