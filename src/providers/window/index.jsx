import { createContext, useContext, useEffect, useState } from "react";

const WindowCTX = createContext();

export const WindowProvider = ({ children }) => {

	const [pageWidth, setPageWidth] = useState(window.innerWidth);

	useEffect(() => {
		window.addEventListener("resize", () => setPageWidth(window.innerWidth));
	}, []);



	return (
		<WindowCTX.Provider
			value={{
				pageWidth,
			}}
		>
			{children}
		</WindowCTX.Provider>
	);
};

export const useWindow = () => useContext(WindowCTX);
