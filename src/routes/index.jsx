import { Route, Switch } from "react-router";
import Dashboard from "../pages/Dashboard";
import Home from "../pages/Home";
import Register from "../pages/Register";
import Login from "../pages/Login";
import Group from "../pages/GroupPage";
import SearchGroupsPage from "../pages/SearchGroupsPage";
import MyGroupsPage from '../pages/MyGroupsPage';

const Routes = () => {
	return (
		<Switch>
			<Route exact path="/" component={Home} />
			<Route path="/register" component={Register} />
			<Route path="/login" component={Login} />
			<Route path="/dashboard" component={Dashboard} />
			<Route exact path="/groups/:id" component={Group} />
			<Route exact path="/searchGroups/groups/:id" component={Group} />
			<Route exact path="/mygroups/groups/:id" component={Group} />
			<Route path="/searchGroups" component={SearchGroupsPage} />
			<Route path="/mygroups" component={MyGroupsPage}/>
			<Route path="*" component={Home} />
		</Switch>
	);
};

export default Routes;
