import * as yup from 'yup';

export const loginSchema = yup.object().shape({
  username: yup.string().required("Campo obrigatório"),
  password: yup
    .string()
    .required("Campo obrigatório"),
});

export const registerSchema = yup.object().shape({
  username: yup.string().required("Campo obrigatório"),
  email: yup.string().email("E-mail inválido").required("Campo obrigatório"),
  password: yup
    .string()
    .min(6, "Mínimo de 6 caracteres")
    .matches(
      /^((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
      "Senha deve conter ao menos uma letra maiúscula, uma letra minúscula, um número e um caracter especial"
    )
    .required("Campo obrigatório"),
});

export const addHabitSchema = yup.object().shape({
  title: yup.string().required('Escolha um nome para seu hábito'),
  category: yup.string().required('De que categoria é seu hábito ?'),
  difficulty: yup.string().required('O quão difícil pra você é fazer esta atividade ?'),
  frequency: yup.string().required('Com que frenquência você pretende fazer esta atividade ?'),
})

export const addGroupsSchema = yup.object().shape({
  name: yup.string().required('Escolha um nome para o seu grupo'),
  description: yup.string().required('Descreva o objetivo do seu grupo'),
  category: yup.string().required('Informe a categoria do seu grupo'),
})

export const addGoalSchema = yup.object().shape({
  title: yup.string().required('Escolha um nome para sua meta'),
  difficulty: yup.string().required('O quão difícil pra você é fazer esta atividade ?')
})

export const addActivitySchema = yup.object().shape({
  title: yup.string().required('Escolha um nome para sua atividade'),
  realization_time: yup.string().required('estabeleca um tempo para sua atividade')
})