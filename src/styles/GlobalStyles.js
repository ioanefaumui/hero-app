import theme from "@chakra-ui/theme";
import { createGlobalStyle } from "styled-components";

const heroTheme = {
  ...theme,
  styles: {
    global: {
      html: {
        fontSize: 16,
      },
      ul: { listStyle: "none" },
      "h1,h2,h3": { fontFamily: "'Poppins', sans-serif;" },
      body: {
        minHeight: "100vh",
        margin: "0 auto",
        backgroundColor: "white",
        fontFamily: "'Nunito', sans-serif",
        paddingTop: "80px",
        paddingBottom: "60px",
      },
    },
  },
  colors: {
    green: { 500: "#319795", 600: "#319795", 300: "#5cb85c" },
    gray: {
      100: "#edf2f7",
      300: "#c4c4c4",
      500: "#757677",
    },
    white: { 500: "#fafcff" },
    red: { 300: "#d9534f", 500: "#f22222" },
    sport: "#c38e8e",
    education: "#8396a8",
    leisure: "#7F9A82",
    health: "#bfb684",
    music: "#b59bb6",
    kitchen: "#d0a780",
    trip: "#83b3bd",
    other: "#c4c4c4",
  },
};

export const GlobalStyles = createGlobalStyle`

/*SCROLL BARS: */
.scrollBar {
	overflow: auto;
}
::-webkit-scrollbar {
	width: 0.5rem;
}
::-webkit-scrollbar-track {
	box-shadow: inset 0 0 5px ${heroTheme.colors.gray[500]};
}
::-webkit-scrollbar-thumb {
	background: ${heroTheme.colors.green[500]};
}
/* ::-webkit-scrollbar-track,
::-webkit-scrollbar-thumb {
} */

.displayNone{display:none}

`;

export default heroTheme;
