export const editTypes = { TYPE_HABIT: 0, TYPE_GOAL: 1, TYPE_ACTIVITY: 2 };

export const USER_TAG = "@Hero:user";
export const TAG_USER_HABITS_LIST = "@Hero:userHabitsList";
export const TAG_USER_GOAL_LIST = "@Hero:userGoalsList";
export const TAG_USER_ACTIVITIES_LIST = "@Hero:userActivitiesList";
export const TAG_USER_GROUPS_LIST = "@Hero:userGroupsList";

export const SCREEN_BREAKPOINT = 900; // pixels

export const toastConfig = {
	DURATION: 1500,
	POSITION: "top-right",
	COLOR: "white.500",
	PADDING: 4,
	DISPLAY: "flex",
	ALIGN_ITEMS: "center",
	MARGIN_RIGHT: 2.5,
	GREEN: "green.300",
	RED: "red.300",
};

export const groupsCategories = {
	HERO_ALL: "@Hero:",
	HERO_EXERCISES: "@Hero:Exercícios",
	HERO_EDUCATION: "@Hero:Educação",
	HERO_HEALTH: "@Hero:Saúde",
	HERO_KITCHEN: "@Hero:Cozinha",
	HERO_LEISURE: "@Hero:Lazer",
	HERO_MUSIC: "@Hero:Música",
	HERO_OTHER: "@Hero:Outra",
	HERO_READING: "@Hero:Leitura",
	HERO_SPORT: "@Hero:Esporte",
	HERO_TRIP: "@Hero:Viagem",
};

export const categoriesColors = {
	"@Hero:Educação": "#8396A8",
	"@Hero:Saúde": "#BFB684",
	"@Hero:Cozinha": "#D0A780",
	"@Hero:Lazer": "#7F9A82",
	"@Hero:Música": "#B59BB6",
	"@Hero:Outra": "#C4C4C4",
	"@Hero:Leitura": "#738ADB",
	"@Hero:Esporte": "#C38E8E",
	"@Hero:Viagem": "#83B3BD",
};

export const difficultyLevels = {
	HERO_VERY_EASY: "@Hero:Muito Fácil",
	HERO_EASY: "@Hero:Fácil",
	HERO_INTERMEDIARY: "@Hero:Intermediário",
	HERO_HARD: "@Hero:Difícil",
	HERO_VERY_HARD: "@Hero:Muito Difícil",
};